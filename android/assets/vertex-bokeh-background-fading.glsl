#ifdef GL_ES
precision mediump float;
#endif
// the above is new

// our attributes. note: a_position is relative to the origin of the light, not the absolute position in space. see next section.
attribute vec3 a_position;
attribute vec2 a_uv;

// our camera matrix
uniform mat4 u_projModelView;

// new
uniform float fade_brightness;


// brightness modifier for fade in and out.
uniform float brightness;

// send the color out to the fragment shader.
varying vec4 vColor;
varying vec2 textureUv;
varying vec3 fragPosition;


void main() {
//	vColor = vec4(1.0, 1.0, 1.0, 1.0) * brightness;
	vColor = vec4(1.0, 1.0, 1.0, 1.0) * brightness * fade_brightness;
    textureUv = a_uv;

	gl_Position = u_projModelView * vec4(a_position.xyz, 1.0);
	fragPosition = gl_Position.xyz;
}
