#ifdef GL_ES
precision mediump float;
#endif

// uniform texture
uniform sampler2D texture;

// new
// uniform float fade_brightness;

//attributes from vertex shader
varying vec4 vColor;
varying vec3 fragPosition;
varying vec2 textureUv;


void main() {
    gl_FragColor = texture2D(texture, textureUv) * vColor; // * fade_brightness;
}

