// our attributes. note: a_position is relative to the origin of the light, not the absolute position in space. see next section.
attribute vec3 a_position;
attribute vec4 a_color;
attribute float a_distance;

// position of the light. this is the position of the origin in space.
uniform vec3 light_position;
uniform float brightness_runtime;

// our camera matrix
uniform mat4 u_projModelView;
uniform float focal_distance;

// send the color out to the fragment shader
varying vec4 vColor;
varying vec3 fragPosition;
varying float Distance;
varying float FocalDistance;
varying float DistanceFromFocalDistance;


void main() {
	vColor = (a_color + vec4(0.1, 0.1, 0.1, 1.0)) * brightness_runtime;

    Distance = a_distance;
    FocalDistance = focal_distance;
    DistanceFromFocalDistance = a_position.z - FocalDistance;

    vec2 resizedLightPosition2d;
    resizedLightPosition2d.x = a_position.x * (light_position.z / 150.0);
    resizedLightPosition2d.y = a_position.y * (light_position.z / 150.0);

    vec3 resizedLightPosition;
    resizedLightPosition = vec3(resizedLightPosition2d.x, resizedLightPosition2d.y, a_position.z);

    vec3 movedPosition = resizedLightPosition + light_position;

	gl_Position = u_projModelView * vec4(movedPosition, 1.0);
	fragPosition = gl_Position.xyz;
}
