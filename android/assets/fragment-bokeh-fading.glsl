#ifdef GL_ES
precision highp float;
#endif

//attributes from vertex shader
varying vec4 vColor;
varying vec3 fragPosition;
varying float Distance;

varying float FocalDistance;
varying float DistanceFromFocalDistance; 

// uniforms
// uniform float fade_brightness;


void main() {

    vec4 finalColor = vColor;

    // we stored the hue shift amount in the alpha channel, so set it to 1.0 after grabbing it
    finalColor.a = 1.0;
    
    float farDistanceNormalised =       ((fragPosition.z - FocalDistance) / FocalDistance)                          * float(fragPosition.z > FocalDistance);

    float middleDistanceNormalised = 0.0;
    middleDistanceNormalised +=    ((FocalDistance - (fragPosition.z - FocalDistance)) / FocalDistance)     * float(fragPosition.z > FocalDistance && fragPosition.z < FocalDistance * 2.0) ;
    middleDistanceNormalised +=    ((FocalDistance - ((FocalDistance - fragPosition.z) * 0.33)) / FocalDistance)     * float(fragPosition.z > 0.0 && fragPosition.z < FocalDistance) ;

    float closeDistanceNormalised =     ((FocalDistance - fragPosition.z) / FocalDistance)                          * float(fragPosition.z < FocalDistance);


    float brightnessFar = (1.0 - Distance) * farDistanceNormalised;
    float brightnessMiddle = middleDistanceNormalised;
    float brightnessClose = pow((1.0 - abs(0.95 - Distance)), (abs(fragPosition.z - FocalDistance) / 4.0)) * closeDistanceNormalised;

    // fudge for taste
   // brightnessFar = (brightnessFar) * 1.0;
   // brightnessMiddle = brightnessMiddle * 1.0;
  //  brightnessClose = (brightnessClose) * 1.0;


    gl_FragColor = (finalColor * brightnessClose + finalColor * brightnessMiddle + finalColor * brightnessFar);

}

