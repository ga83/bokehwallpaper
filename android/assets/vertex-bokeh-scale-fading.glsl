// our attributes. note: a_position is relative to the origin of the light, not the absolute position in space. see next section.
attribute vec3 a_position;
attribute vec4 a_color;
attribute float a_distance;

// position of the light. this is the position of the origin in space.
uniform vec3 light_position;
uniform float brightness_runtime;

// our camera matrix
uniform mat4 u_projModelView;
uniform float focal_distance;

uniform float fade_brightness;

// send the color out to the fragment shader
varying vec4 vColor;
varying vec3 fragPosition;
varying float Distance;
varying float FocalDistance;
varying float DistanceFromFocalDistance;


void main() {
	vColor = (a_color + vec4(0.1, 0.1, 0.1, 1.0)) * brightness_runtime * fade_brightness;

    Distance = a_distance;
    FocalDistance = focal_distance;
    DistanceFromFocalDistance = a_position.z - FocalDistance;

    vec3 movedPosition = a_position + light_position;
	gl_Position = u_projModelView * vec4(movedPosition, 1.0);
	fragPosition = gl_Position.xyz;
}
