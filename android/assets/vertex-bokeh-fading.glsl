// our attributes. note: a_position is relative to the origin of the light, not the absolute position in space. see next section.
attribute vec3 a_position;
attribute vec4 a_color;
attribute float a_distance;

// position of the light. this is the position of the origin in space.
uniform vec3 position;

uniform float fade_brightness;

// our camera matrix
uniform mat4 u_projModelView;
uniform float focal_distance;

// send the color out to the fragment shader
varying vec4 vColor;
varying vec3 fragPosition;
varying float Distance;
varying float FocalDistance;
varying float DistanceFromFocalDistance;


void main() {
	vColor = a_color * fade_brightness;
    Distance = a_distance;
    FocalDistance = focal_distance;
    DistanceFromFocalDistance = a_position.z - FocalDistance;
	
	gl_Position = u_projModelView * vec4(a_position.xyz, 1.0);
	fragPosition = gl_Position.xyz;
}
