package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.math.Vector3;

public class AnyStraightConstantMotionStrategy extends MotionStrategy {
    private final Vector3 initialAcceleration;
    private final float motionSpeed;

    public AnyStraightConstantMotionStrategy(float motionSpeed) {
        this.motionSpeed = motionSpeed;
        this.initialAcceleration = new Vector3(0,0,0);
    }

    public void initialise(Light light, float dt) {
        light.velocity = new Vector3().setToRandomDirection().nor().scl(motionSpeed);
        light.acceleration = this.initialAcceleration.cpy();
    }

    @Override
    public void update(Light light, float dt) {
    }

}
