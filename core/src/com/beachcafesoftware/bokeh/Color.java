package com.beachcafesoftware.bokeh;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 23/08/17.
 */
public class Color {

    private static Random rnd = new Random();

    public static String[] SCHEMES_TYPE_2 = new String[] { "time","timetripod","timebipod" };
    public static String[] SCHEMES_TYPE_3 = new String[] { "bluereddiscrete", "redgreenblue", "beach", "goldsilverbronze", "fluro", "all" };
    public static String[] SCHEMES_TYPE_4 = new String[] { "redyellow", "yellowgreen", "greencyan", "cyanblue", "bluemagenta", "magentared" };
    public static String[] SCHEMES_TYPE_5 = new String[] { "white" };


    // TODO: colorParams was changed to colorParams[0] as a quick fix. fix it properly.
    public static float[] getRandomColorFromScheme(String[] colorSchemes, boolean returnHsl, boolean maxSat) {

        String randomParameter = colorSchemes[rnd.nextInt(colorSchemes.length)];

        float saturation;
        float lightness;

        if(maxSat == true) {
            saturation = 1.0f;
            lightness = 0.5f;
        } else {
            saturation = 0.5f + rnd.nextFloat() * 0.5f;
            lightness = rnd.nextFloat() * 0.5f;
        }

        if(arrayContainsString(SCHEMES_TYPE_2, randomParameter)) {   // TODO

            if(randomParameter.equals("time") == true) {
                float hue = getBase();
                hue += rnd.nextFloat() * 0.33f;
                if(hue > 1.0f) { hue -= 1.0f; }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }
            if(randomParameter.equals("timetripod") == true) {
                float hue = getBase();
                hue += rnd.nextFloat() * 0.05f;
                int random = rnd.nextInt(3);
                if(random == 1) { hue += 0.33f; }
                if(random == 2) { hue += 0.66f; }
                if(hue > 1.0f) { hue -= 1.0f; }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }
            if(randomParameter.equals("timebipod") == true) {
                float hue = getBase();
                hue += rnd.nextFloat() * 0.05f;
                int random = rnd.nextInt(2);
                if(random == 1) { hue += 0.5f; }
                if(hue > 1.0f) { hue -= 1.0f; }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

        }

        else if(arrayContainsString(SCHEMES_TYPE_3, colorSchemes[0])) {    // TODO

            if(randomParameter.equals("redgreenblue") == true) {
                float hue;

                int rgb = rnd.nextInt(3);

                if(rgb == 0) {
                    hue = 0.0f;
                }
                else if(rgb == 1) {
                    hue = 0.33f;
                }
                else {
                    hue = 0.66f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

            else if(randomParameter.equals("bluereddiscrete") == true) {
                float hue;

                int rgb = rnd.nextInt(2);

                if(rgb == 0) {
                    hue = 0.66f;
                }
                else {
                    hue = 0.0f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

            else if(randomParameter.equals("beach") == true) {
                float hue;

                int rgb = rnd.nextInt(2);

                if(rgb == 0) {
                    hue = 0.66f + rnd.nextFloat() * -0.2f;
                }
                else {
                    hue = 0.16f;
                    lightness = 0.5f + rnd.nextFloat() * 0.4f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }

            }

            else if(randomParameter.equals("goldsilverbronze") == true) {
                float hue;

                int rgb = rnd.nextInt(3);

                if(rgb == 0) {
                    hue = 0.138f;
//                    saturation = 0.91f;
                    //                  lightness = 0.65f;
                    saturation = 0.99f;
                    lightness = 0.5f;

                }
                else if(rgb == 1) {
                    hue = 0.0f;
                    lightness = 0.85f;
                    saturation = 0.0f;
                }
                else {
                    hue = 0.075f;
                    saturation = 0.64f;
                    lightness = 0.69f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

            else if(randomParameter.equals("fluro") == true) {
                float hue;

                int rgb = rnd.nextInt(4);

                // yellow
                if(rgb == 0) {
                    hue = 0.333f / 2f;
                    saturation = 1f;
                    lightness = 0.5f;
                }

                // green
                else if(rgb == 1) {
                    hue = 0.333f;
                    saturation = 1.0f;
                    lightness = 0.5f;
                }

                // orange
                else if(rgb == 2) {
                    hue = 0.333f / 2f / 2f;
                    saturation = 1.0f;
                    lightness = 0.5f;
                }

                // pink
                else {
                    hue = 297 / 360f;
                    saturation = 0.8f;
                    lightness = 0.64f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

            else if(randomParameter.equals("all") == true) {
                float hue = rnd.nextFloat();

                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }


        }

        else if(arrayContainsString(SCHEMES_TYPE_4, colorSchemes[0])) {   // TODO

            float hueMin;
            float oneSixth = 1.0f / 6.0f;

            if(randomParameter.equals("redyellow") == true) {
                hueMin = 0 * oneSixth;
            } else if(randomParameter.equals("yellowgreen") == true) {
                hueMin = 1 * oneSixth;
            } else if(randomParameter.equals("greencyan") == true) {
                hueMin = 2 * oneSixth;
            } else if(randomParameter.equals("cyanblue") == true) {
                hueMin = 3 * oneSixth;
            } else if(randomParameter.equals("bluemagenta") == true) {
                hueMin = 4 * oneSixth;
            } else {
                hueMin = 5 * oneSixth;
            }

            float hue = hueMin + rnd.nextFloat() * oneSixth;

            System.out.println("saturation: " + saturation + ", lightness: " + lightness);

            if(returnHsl) {
                return new float[] { hue, saturation, lightness };
            } else {
                return hslToRgb(hue, saturation, lightness);
            }

        }

        else if(arrayContainsString(SCHEMES_TYPE_5, colorSchemes[0])) {    // TODO
            float hue = 0.0f;
            saturation = 0.0f;
            lightness = 1.0f;

            if(returnHsl) {
                return new float[] { hue, saturation, lightness };
            } else {
                return hslToRgb(hue, saturation, lightness);
            }

        }

            return null;
    }

    private static boolean arrayContainsString(String[] strings, String string) {
        for(int i = 0; i < strings.length; i++) {
            if(strings[i].equals(string) == true) {
                return true;
            }
        }
        return false;
    }

    private static float getBase() {
        // 100 buckets per day
        int SECONDS_PER_BUCKET = (60 * 60 * 24) / 100;
        Calendar c = Calendar.getInstance();
        int utcOffset = c.get(Calendar.ZONE_OFFSET) + c.get(Calendar.DST_OFFSET);
        Long utcMilliseconds = c.getTimeInMillis() + utcOffset;
        float base = ((utcMilliseconds / (1000 * SECONDS_PER_BUCKET)) % 100) / 100f;
        return base;
    }

    public static float[] hslToRgb(float hue, float sat, float lum) {
        float r;
        float g;
        float b;

        if (sat == 0) {
            r = g = b = lum; // achromatic
        } else {
            float q = lum < 0.5f ? lum * (1 + sat) : lum + sat - lum * sat;
            float p = 2 * lum - q;
            r = hue2rgb(p, q, hue + 1f / 3f);
            g = hue2rgb(p, q, hue);
            b = hue2rgb(p, q, hue - 1f / 3f);
        }
        return new float[] { r , g , b };
    }


    public static float[] getRandomColorFromSchemeDarkened(String[] colorScheme, boolean maxSat) {
        float[] color = getRandomColorFromScheme(colorScheme, false, maxSat);
        color[0] *= 0.2f;
        color[1] *= 0.2f;
        color[2] *= 0.2f;
        return color;
    }


    public static float[] getRandomColorFromSchemeHalfDarkened(String[] colorScheme, boolean returnHsl, boolean maxSat) {
        float[] color = getRandomColorFromScheme(colorScheme, returnHsl, maxSat);

        if(returnHsl == false) {
            color[0] *= 0.01f;
            color[1] *= 0.01f;
            color[2] *= 0.01f;
        }
        else {
            color[2] *= 0.01f;
        }

        return color;
    }


    public static float hue2rgb(float p, float q, float t) {
        if (t < 0f) t += 1f;
        if (t > 1f) t -= 1f;
        if (t < 1f / 6f) return p + (q - p) * 6f * t;
        if (t < 1f / 2f) return q;
        if (t < 2f / 3f) return p + (q - p) * (2f / 3f - t) * 6f;
        return p;
    }


    public static float[] lerpRgb(float[] low, float[] high, float position) {
        float dr = high[0] - low[0];
        float dg = high[1] - low[1];
        float db = high[2] - low[2];

        float r = low[0] + dr * position;
        float g = low[1] + dg * position;
        float b = low[2] + db * position;

        float[] interpolated = new float[] { r, g, b };

        return interpolated;
    }


}
