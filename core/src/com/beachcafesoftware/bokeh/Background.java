package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

public class Background {

    // might be as simple as 2 triangles
    public VboDataRetained vboDataRetained;

    public Background(Pixmap gradientPixmap, KirraArrayList<Triangle> triangles) {
        Texture texture = new Texture(gradientPixmap);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        gradientPixmap.dispose();

        this.vboDataRetained = new VboDataRetained(triangles, texture);
    }

    public void dispose() {
        this.vboDataRetained.dispose(true, "");
    }
}
