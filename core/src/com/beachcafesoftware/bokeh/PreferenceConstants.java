package com.beachcafesoftware.bokeh;

public class PreferenceConstants {

    /**
     * In general, this class is required because android preferences can only be written as scalar values.
     * So each "value" stored in preferences is actually an index into these arrays.
     * This is a highly abstract solution which is flexible. It allows for references to ranges, and anything else.
     */

    // The first dimension is a discrete bucket. The second dimension is a minimum [0] and maximum [1].
    static float[][] motionSpeeds;
    static float[][] numberOfLights;
    static float[][] diameters;
    static float[][] lifetimes;
    static float[][] lightBrightness;
    static float[][] backgroundBrightness;
    static float[][] frequencies;
    static float[][] sides;
    static String[] motions;
    static boolean[] scales;
    static float[] focalDistances;
    static int[] foregroundColorSetSize;


    static {
            PreferenceConstants.motionSpeeds = new float[][]{
                {0.0f, 0.0f},
                {1.0f, 2.0f},
                {2.0f, 4.0f},
                {4.0f, 8.0f},
                {8.0f, 16.0f},
                {16.0f, 32.0f}
            };

            PreferenceConstants.numberOfLights = new float[][] {
                { 2.0f, 3.0f },
                { 3.0f, 6.0f },
                { 6.0f, 9.0f },
                { 9.0f, 12.0f },
                { 12.0f, 15.0f },
                { 40.0f, 40.0f }
           //     { 160.0f, 160.0f }
            };

            PreferenceConstants.diameters = new float[][] {
                    { 3.0f, 7.0f },
                    { 7.0f, 12.0f },
                    { 12.0f, 17.0f },
                    { 17.0f, 22.0f },
                    { 22.0f, 27.0f },
                    { 27.0f, 32.0f },
                    { 32.0f, 37.0f }
            };

            PreferenceConstants.lifetimes = new float[][] {
                    { 1.5f, 4.5f },
                    { 4.5f, 7.5f },
                    { 7.5f, 10.5f },
                    { 10.5f, 13.5f },
                    { 13.5f, 16.5f }
            };

            PreferenceConstants.lightBrightness = new float[][] {
                    { 0.000f, 0.125f },
                    { 0.125f, 0.250f },
                    { 0.250f, 0.375f },
                    { 0.375f, 0.500f },
                    { 0.500f, 0.625f },
                    { 0.625f, 0.750f },
                    { 0.750f, 0.875f },
                    { 0.875f, 1.000f }
            };

        PreferenceConstants.backgroundBrightness = new float[][] {
                { 0.000f, 0.125f },
                { 0.125f, 0.250f },
                { 0.250f, 0.375f },
                { 0.375f, 0.500f },
                { 0.500f, 0.625f },
                { 0.625f, 0.750f },
                { 0.750f, 0.875f },
                { 0.875f, 1.000f }
        };

        PreferenceConstants.frequencies = new float[][] {
                { 0.00f, 0.00f },
                { 0.01f, 1.00f },
                { 1.00f, 2.00f },
                { 2.00f, 3.00f },
                { 3.00f, 5.00f },
                { 5.00f, 8.00f },
        };

        PreferenceConstants.sides = new float[][] {
                { 5.0f, 5.1f },
                { 6.0f, 6.1f },
                { 7.0f, 7.1f },
                { 8.0f, 8.1f },
                { 9.0f, 9.1f },
                { 128.0f, 128.1f }
        };

        PreferenceConstants.motions = new String[] {
                "down_straight_constant",
                "any_straight_constant",
                "insect",
                "starfield"
        };

        PreferenceConstants.scales = new boolean[] {
                false,
                true
        };

        PreferenceConstants.focalDistances = new float[] {
                50.0f,
                100.0f,
                150.0f,
                250.0f,
                300.0f
        };

        PreferenceConstants.foregroundColorSetSize = new int[] {
                1,
                2,
                3,
                4,
                5,
                6
        };
    }
}
