package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

public abstract class FrameBufferBasedRenderer implements BokehRenderer {

    FrameBuffer fb;


    protected void initialiseFrameBuffer(String imageQuality, int width, int height) {
        if (imageQuality.equals("high") == true) {
            // normal
            fb = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
        } else if (imageQuality.equals("medium") == true) {
            fb = new FrameBuffer(Pixmap.Format.RGBA8888, width / 2, height / 2, false);
        } else if (imageQuality.equals("low") == true) {
            fb = new FrameBuffer(Pixmap.Format.RGBA8888, width / 3, height / 3, false);
        } else if (imageQuality.equals("verylow") == true) {
            fb = new FrameBuffer(Pixmap.Format.RGBA8888, width / 4, height / 4, false);
        } else if (imageQuality.equals("extremelylow") == true) {
            fb = new FrameBuffer(Pixmap.Format.RGBA8888, width / 8, height / 8, false);
        }


    }
}
