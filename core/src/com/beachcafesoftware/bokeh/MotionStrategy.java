package com.beachcafesoftware.bokeh;

public abstract class MotionStrategy {

    public static MotionStrategy getStrategy(String strategyDescription, float speed) {
        if(strategyDescription.equals("down_straight_constant") == true) {
            return new DownStraightConstantMotionStrategy(speed);
        } else if(strategyDescription.equals("any_straight_constant") == true) {
            return new AnyStraightConstantMotionStrategy(speed);
        } else if(strategyDescription.equals("starfield") == true) {
            return new TowardsStraightFixedMotionStrategy(speed);
        } else if(strategyDescription.equals("insect") == true) {
            return new InsectMotionStrategy(speed);
        }

        // default, should never be reached, unless preferences are corrupted
        return new DownStraightConstantMotionStrategy(speed);
    }

    public abstract void initialise(Light light, float dt);
    public abstract void update(Light light, float dt);

}
