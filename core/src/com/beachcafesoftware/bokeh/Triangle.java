package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Triangle {

    // vertex positions in world space
    public Vector3 v1;
    public Vector3 v2;
    public Vector3 v3;

    public Vector2 uv1;
    public Vector2 uv2;
    public Vector2 uv3;

    public Color color;


    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, Vector2 uv1, Vector2 uv2, Vector2 uv3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        this.uv1 = uv1;
        this.uv2 = uv2;
        this.uv3 = uv3;
    }

    public Triangle(Vector3 vertex1, Vector3 vertex2, Vector3 vertex3, Color color) {
        this.v1 = vertex1;
        this.v2 = vertex2;
        this.v3 = vertex3;
        this.color = color;
    }
}
