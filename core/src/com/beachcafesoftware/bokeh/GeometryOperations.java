package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GeometryOperations {


    public static KirraArrayList<Triangle> getBackgroundTriangles(float squareSideLength, float distance) {

        Vector3 v1;
        Vector3 v2;
        Vector3 v3;
        Vector2 uv1;
        Vector2 uv2;
        Vector2 uv3;

        Triangle triangle;
        KirraArrayList<Triangle> triangles = new KirraArrayList<Triangle>(2);

        v1 = new Vector3(-squareSideLength / 2, -squareSideLength / 2, distance);
        v2 = new Vector3(squareSideLength / 2, -squareSideLength / 2, distance);
        v3 = new Vector3(squareSideLength / 2, squareSideLength / 2, distance);

        uv1 = new Vector2(0, 0);
        uv2 = new Vector2(1, 0);
        uv3 = new Vector2(1, 1);

        triangle = new Triangle(v1, v2, v3, uv1, uv2, uv3);

        triangles.add(triangle);

        v1 = new Vector3(squareSideLength / 2, squareSideLength / 2, distance);
        v2 = new Vector3(-squareSideLength / 2, squareSideLength / 2, distance);
        v3 = new Vector3(-squareSideLength / 2, -squareSideLength / 2, distance);

        uv1 = new Vector2(1, 1);
        uv2 = new Vector2(0, 1);
        uv3 = new Vector2(0, 0);

        triangle = new Triangle(v1, v2, v3, uv1, uv2, uv3);

        triangles.add(triangle);

        return triangles;
    }

}
