package com.beachcafesoftware.bokeh;

public class ShaderParametersKey {

    // light, background, etc.
    public String type;
    public boolean scaling;
    public boolean fading;


    public ShaderParametersKey(String type, boolean scaling, boolean fading) {
        this.type = type;
        this.scaling = scaling;
        this.fading = fading;
    }

    @Override
    public int hashCode() {
        int typeHash = this.type.hashCode();
        int scalingHash = 1 * (this.scaling ? 1 : 0);
        int fadingHash = 2 * (this.fading ? 1 : 0);
        return typeHash ^ scalingHash ^ fadingHash;
    }

    @Override
    public boolean equals(Object object) {
        ShaderParametersKey shaderParametersKey = (ShaderParametersKey)object;
        return shaderParametersKey.type.equals(this.type) &&
                shaderParametersKey.scaling == this.scaling &&
                shaderParametersKey.fading == this.fading;
    }
}
