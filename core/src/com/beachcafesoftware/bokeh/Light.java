package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by Guy Aaltonen on 27/01/19.
 */

public class Light {

    public Vector3 position;
    public Vector3 velocity;
    public Vector3 acceleration;
    // public VertexBufferObject vertexBufferObject;
    public VboDataRetained vboDataRetained;

    // every 3 vertices, in order, is a triangle. thus this list should always be of size multiple of 3.
    // always centered around 0. not used for rendering.
    public KirraArrayList<Vector3> vertices;

    // never centered around 0, except when this light is brand new. always modified by position. used for rendering.
    public KirraArrayList<Vector3> translatedVertices;
    public boolean renderLight;

    // an injected strategy which implements the movement from frame to frame.
    private MotionStrategy motionStrategy;

    public float initialLife;
    public float life;
    public float r;
    public float g;
    public float b;
    public float brightnessInitial;
    public float brightnessRuntime;

//    public float speed;
    public float[] periodicFrequencies;

    public Light(MotionStrategy motionStrategy) {
        this.motionStrategy = motionStrategy;
    }

    public void updateMotion(float dt) {
        this.motionStrategy.update(this, dt);
    }

    public void updateVelocity() {
        this.velocity.add(this.acceleration);
    }

    public void updatePosition(float dt) {
        float dx = this.velocity.x * dt;
        float dy = this.velocity.y * dt;
        float dz = this.velocity.z * dt;
        this.position.add(dx, dy, dz);
    }

    public void allocateVertices(int numSides) {
        this.vertices = new KirraArrayList<Vector3>(numSides * 3);
        this.translatedVertices = new KirraArrayList<Vector3>(numSides * 3);
    }

    public void updateLife(float dt) {
        this.life -= dt;
    }

    public void dispose() {
        this.vboDataRetained.dispose(false, "");
    }
}
