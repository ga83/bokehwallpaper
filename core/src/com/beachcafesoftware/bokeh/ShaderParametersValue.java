package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * This class contains references to the files, and the ShaderProgram itself, and is the mapping for any number of input parameters such as scaling and fading.
 * The main benefit is to cache the ShaderProgram, which would be expensive to recompile on every scene change.
 */
public class ShaderParametersValue {

    public String vertexShaderFileName;
    public String fragmentShaderFileName;
    public ShaderProgram shaderProgram;

    public ShaderParametersValue(String vertexShaderFileName, String fragmentShaderFileName) {
        this.vertexShaderFileName = vertexShaderFileName;
        this.fragmentShaderFileName = fragmentShaderFileName;
    }

}
