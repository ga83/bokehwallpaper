package com.beachcafesoftware.bokeh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;



public class KirraArrayList<T> {

    public Object[] backingArray;
    public int endIndex;


    public int hashCode() {
        return Arrays.hashCode(this.backingArray);
    }

    /*
    // TODO: double check that this is sufficient. may not be a problem for us, but may be.
    public boolean equalHashes(Object list2) {
        return this.hashCode() == list2.hashCode();
    }
    */

    public boolean equals(Object list2) {
        if(list2 == null) {
            return false;
        }

        // if not another KirraArrayList, definitely not equal.
        if(list2 instanceof KirraArrayList == false) {

            // System.out.println("====== NOT INSTANCE OF KIRRAARRAYLIST");
            System.exit(1);
           // throw new RuntimeException("aaa bot linesSet cannot compare another class' object to KirraArrayList");
        }

        // if different length, definitely not equal.
        if(((KirraArrayList)list2).size() != this.size()) {
            return false;
        }

        // if any element is different to the corresponding element, definitely not equal.
        int size = ((KirraArrayList)list2).size();
        for(int i = 0; i < size; i++) {
            if(this.get(i).equals(((KirraArrayList)list2).get(i)) == false) {
                return false;
            }
        }

        // got this far? then equal.
        return true;
    }

    public KirraArrayList(HashSet<T> hashSet, int finalSize) {
        this.backingArray = (T[])(new Object[finalSize]);
        this.addAll(hashSet);
    }


    public KirraArrayList(int finalSize) {
        this.backingArray = (T[])(new Object[finalSize]);
    }

    public KirraArrayList(KirraArrayList<T> subHighestValuePotentialLine) {
        this.endIndex = 0;
        this.backingArray = (T[])(new Object[subHighestValuePotentialLine.backingArray.length]);
//        this.addAll(subHighestValuePotentialLine);

        for(int i = 0; i < subHighestValuePotentialLine.backingArray.length; i++) {
            this.backingArray[i] = subHighestValuePotentialLine.backingArray[i];
        }

        this.endIndex = subHighestValuePotentialLine.endIndex;
    }


    public HashSet<T> asSet() {
        HashSet<T> hashSet = new HashSet<T>();

        for(int i = 0; i < this.endIndex; i++) {
            hashSet.add((T)this.backingArray[i]);
        }

        return hashSet;
    }


    public void addAll(HashSet<T> hashSet) {
        T[] tArray = (T[])(new ArrayList<T>(hashSet)).toArray();
        int numToCopy = tArray.length;
        System.arraycopy(tArray, 0, this.backingArray, this.endIndex, numToCopy);
        this.endIndex += numToCopy;
    }


    public void addAll(KirraArrayList<T> kirraArrayList) {
        int numToCopy = kirraArrayList.endIndex;
        System.arraycopy(kirraArrayList.backingArray, 0, this.backingArray, this.endIndex, numToCopy);
        this.endIndex += numToCopy;
    }


    public void clear() {
        System.arraycopy(this.backingArray, 0, this.backingArray, 0, this.endIndex);
        this.endIndex = 0;
    }

    public void removeByAddress(T item) {
        for(int i = 0; i < this.endIndex; i++) {
             if(this.backingArray[i] == item) {
                this.remove(i);
            }
        }
    }


    public void remove(T item) {
        for(int i = 0; i < this.endIndex; i++) {
            // if(this.backingArray[i] == item) {
            // must compare by equals
            if(item.equals(this.backingArray[i]) == true) {
                this.remove(i);
            }
        }
    }

    public void remove(int index) {
        int numToCopy = this.endIndex - index - 1;

        if (numToCopy > 0) {
            System.arraycopy(this.backingArray, index + 1, this.backingArray, index, numToCopy);
        }
        this.backingArray[--this.endIndex] = null;
    }


    public void add(T item) {
        // TODO: remove this check when we're sure that the backing array is large enough.
        if(this.endIndex > this.backingArray.length) {
            //throw new RuntimeException("backing array too small: " + this.backingArray.length);
            System.out.println("====== backing array too small");
            System.exit(1);
        }

        this.backingArray[this.endIndex] = item;
        this.endIndex ++;
    }


    public T get(int i) {
        return (T)this.backingArray[i];
    }


    public int size() {
        return this.endIndex;
    }


    public void set(int i, T item) {
        this.backingArray[i] = item;
    }


    public void sort(Comparator<T> comparator) {
        Arrays.sort((T[])this.backingArray, 0, this.endIndex, comparator);
    }


    public String printList() {
        String output = "";

        for(int i = 0; i < this.size(); i++) {
            output += this.get(i) + ", ";
        }

        return output;
    }

    public boolean contains(T item) {
        return this.indexOf(item) >= 0;
    }

    public int indexOf(T item) {
        if (item == null) {
            for (int i = 0; i < this.endIndex; i++)
                if (this.backingArray[i] == null)
                    return i;
        } else {
            for (int i = 0; i < this.endIndex; i++) {
                if (item.equals(this.backingArray[i])) {
                    return i;
                }
            }
        }

        return -1;
    }

    public void truncate(int length) {
        length = Math.min(this.endIndex, length);

        this.endIndex = length;
        T[] newbackingArray = (T[]) (new Object[length]);
        System.arraycopy(this.backingArray, 0, newbackingArray, 0, length);
        this.backingArray = newbackingArray;
    }

    public void truncateByLength(int i) {
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> arrayList = new ArrayList<T>();

        for(int i = 0; i < this.size(); i++) {
            arrayList.add((T)this.backingArray[i]);
        }

        return arrayList;
    }
}
