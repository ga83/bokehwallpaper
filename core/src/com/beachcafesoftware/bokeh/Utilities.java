package com.beachcafesoftware.bokeh;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

public class Utilities {

    /**
     * Get a random index from an array of indices.
     */
    public static int getRandomIndexFromIndices(int[] selectedBucketIndices, Random rnd) {
        int bucketIndexIndex = rnd.nextInt(selectedBucketIndices.length);
        int bucketIndex = selectedBucketIndices[bucketIndexIndex];
        return bucketIndex;
    }

    /**
     * Get a random value from range bucket.
     */
    public static float getRandomValueFromRangeBucket(float[][] allBuckets, int[] selectedBucketIndices, boolean log, Random rnd) {

        // choose one out of the selected buckets.
        int bucketIndexIndex = rnd.nextInt(selectedBucketIndices.length);
        int bucketIndex = selectedBucketIndices[bucketIndexIndex];

        float valueMinimum = allBuckets[bucketIndex][0];
        float valueMaximum = allBuckets[bucketIndex][1];
        float valueMedian = (valueMinimum + valueMaximum) / 2.0f;

       // if(log == true){
          //  System.out.println("===== valueMedian: " + valueMedian);
      //  }

        float randomRangeNormalised1 = 1.0f;
        float value = Utilities.getRandomisedValue(valueMedian, randomRangeNormalised1, valueMinimum, valueMaximum, rnd);

      //  if(log == true) {
       //     System.out.println("===== value: " + value);
      //  }

        return value;
    }

    public static Map<String, ?> getMapOfStringToSetOfStringsFromStringArray(String key, String[] value) {
        Map<String, HashSet<String[]>> map = new HashMap<String, HashSet<String[]>>();
        HashSet hashSet = new HashSet<String>();

        for(int i = 0; i < value.length; i++) {
            hashSet.add(value[i]);
        }

        map.put(key, hashSet);

        return map;
    }

    /**
     * Returns the given value but with a randomised offset either below or above it, the range of which is specified by the range.
     * Values are clamped between supplied minValue and maxValue.
     * The values generated form a uniform distribution.
     */
    public static float getRandomisedValue(float median, float normalisedRange, float minValue, float maxValue, Random rnd) {
        float concreteRange = maxValue - minValue;
        float halfRange = (normalisedRange * concreteRange) / 2.0f;
        float sign = rnd.nextBoolean() ? -1.0f : 1.0f;
        float unclampedValue = median + rnd.nextFloat() * halfRange * sign;
        float clampedValue = Math.max(minValue, Math.min(maxValue, unclampedValue));

        return clampedValue;
    }


    public static String arrayToString(String[] stringArray) {
        String string = "";
        for(int i = 0; i < stringArray.length; i++) {
            string += stringArray[i] + ", ";
        }
        return string;
    }
}
