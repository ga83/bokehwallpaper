package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;

public class VboDataRetained extends VboData {

    // this subclass is all about this member... this object allows us to create a retained VBO which is faster because it resides on the GPU.
    public VertexBufferObject vertexBufferObject;


    public VboDataRetained() {
        super();
    }

    public void dispose(boolean disposeTexture, String whereFrom) {
        // System.out.println("===== disposing vboData from " + whereFrom);

        // TODO: this null check and assignment to null might not be correct.
        if(this.vertexBufferObject != null) {
            this.vertexBufferObject.dispose();

            // TODO: #38389 THIS EXPOSES THAT SOMETHING IS REFERRING TO THIS VBO STILL THAT SHOULDN'T BE. KEEP IT, BUT FIND OUT WHY SOMETHING IS REFERRING TO THIS WHEN IT SHOULDN'T BE.
            this.vertexBufferObject = null;
        }

        if(disposeTexture == true) {

            // TODO: this null check and assignment to null might not be correct.
            if(this.texture != null) {
                this.texture.dispose();
                this.texture = null;
            }
        }
    }

    /**
     * VBO constructor for background. A texture is passed in.
     * @param triangles
     */
    public VboDataRetained(KirraArrayList<Triangle> triangles, Texture texture) {

        this.triangles = triangles;
        this.texture = texture;


        this.createVboData(triangles);
    }

    public void createVboData(KirraArrayList<Triangle> triangles) {

        float[] vboVerts = new float[triangles.size() * 3 * BokehRendererRetainedShader.NUMBER_OF_COMPONENTS_BACKGROUND];

        VertexAttribute vA = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_position" );
        VertexAttribute vT = new VertexAttribute( VertexAttributes.Usage.TextureCoordinates, 2, "a_uv" );
        VertexAttributes vboVAs = new VertexAttributes(new VertexAttribute[] { vA, vT });
        VertexBufferObject vbo = new VertexBufferObject(true, vboVerts.length / BokehRendererRetainedShader.NUMBER_OF_COMPONENTS_BACKGROUND, vboVAs);

        this.vboIndex = 0;

        for(int i = 0; i < triangles.size(); i++) {
            Triangle triangle = triangles.get(i);

            // vertex 1
            // position
            vboVerts[this.vboIndex++] = triangle.v1.x;
            vboVerts[this.vboIndex++] = triangle.v1.y;
            vboVerts[this.vboIndex++] = triangle.v1.z;

            // uv 1
            vboVerts[this.vboIndex++] = triangle.uv1.x;
            vboVerts[this.vboIndex++] = triangle.uv1.y;

            // vertex 2
            // position
            vboVerts[this.vboIndex++] = triangle.v2.x;
            vboVerts[this.vboIndex++] = triangle.v2.y;
            vboVerts[this.vboIndex++] = triangle.v2.z;

            // uv 2
            vboVerts[this.vboIndex++] = triangle.uv2.x;
            vboVerts[this.vboIndex++] = triangle.uv2.y;

            // vertex 3
            // position
            vboVerts[this.vboIndex++] = triangle.v3.x;
            vboVerts[this.vboIndex++] = triangle.v3.y;
            vboVerts[this.vboIndex++] = triangle.v3.z;

            // uv 3
            vboVerts[this.vboIndex++] = triangle.uv3.x;
            vboVerts[this.vboIndex++] = triangle.uv3.y;
        }

        this.vboVerts = vboVerts;
        this.vertexBufferObject = vbo;

        // the key. this puts the vertices on the GPU which greatly improves performance.
        this.vertexBufferObject.setVertices(this.vboVerts, 0, this.vboVerts.length);
    }

    /**
     * VBO constructor for lights. No texture is passed in.
     */
    public VboDataRetained(KirraArrayList<Triangle> triangles) {
        this.triangles = triangles;

        float[] vboVerts = new float[triangles.size() * 3 * BokehRendererRetainedShader.NUMBER_OF_COMPONENTS];

        VertexAttribute vA = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_position" );
        VertexAttribute vC = new VertexAttribute( VertexAttributes.Usage.ColorUnpacked, 4, "a_color" );
        VertexAttribute vD = new VertexAttribute( VertexAttributes.Usage.ColorUnpacked, 1, "a_distance" );
        VertexAttributes vboVAs = new VertexAttributes(new VertexAttribute[] { vA, vC, vD });

        VertexBufferObject vbo = new VertexBufferObject(true, vboVerts.length / BokehRendererRetainedShader.NUMBER_OF_COMPONENTS, vboVAs);

        this.vboIndex = 0;

        for(int i = 0; i < triangles.size(); i++) {
            Triangle triangle = triangles.get(i);
            float r = triangle.color.r;
            float g = triangle.color.g;
            float b = triangle.color.b;
            float a = triangle.color.a;

            // vertex 1
            // position
            vboVerts[this.vboIndex++] = triangle.v1.x;
            vboVerts[this.vboIndex++] = triangle.v1.y;
            vboVerts[this.vboIndex++] = triangle.v1.z;

            // color
            vboVerts[this.vboIndex++] = r; 	                    // Color(r, g, b)
            vboVerts[this.vboIndex++] = g;
            vboVerts[this.vboIndex++] = b;
            vboVerts[this.vboIndex++] = a;

            // distance
            vboVerts[this.vboIndex++] = 0f;

            // vertex 2
            // position
            vboVerts[this.vboIndex++] = triangle.v2.x;
            vboVerts[this.vboIndex++] = triangle.v2.y;
            vboVerts[this.vboIndex++] = triangle.v2.z;

            // color
            vboVerts[this.vboIndex++] = r; 	                    // Color(r, g, b)
            vboVerts[this.vboIndex++] = g;
            vboVerts[this.vboIndex++] = b;
            vboVerts[this.vboIndex++] = a;

            // distance
            vboVerts[this.vboIndex++] = 1f;

            // vertex 3
            // position
            vboVerts[this.vboIndex++] = triangle.v3.x;
            vboVerts[this.vboIndex++] = triangle.v3.y;
            vboVerts[this.vboIndex++] = triangle.v3.z;

            // color
            vboVerts[this.vboIndex++] = r; 	                    // Color(r, g, b)
            vboVerts[this.vboIndex++] = g;
            vboVerts[this.vboIndex++] = b;
            vboVerts[this.vboIndex++] = a;

            // distance
            vboVerts[this.vboIndex++] = 1f;
        }

        this.vboVerts = vboVerts;
        this.vertexBufferObject = vbo;

        // the key. this puts the vertices on the GPU which greatly improves performance.
        this.vertexBufferObject.setVertices(this.vboVerts, 0, this.vboVerts.length);

    }

}
