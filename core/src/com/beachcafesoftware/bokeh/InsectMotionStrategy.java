package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.math.Vector3;

import java.util.Random;

public class InsectMotionStrategy extends MotionStrategy {

    private static final float BASE_PROBABILITY = 0.05f;
    private final Vector3 initialAcceleration;
    private final float motionSpeed;
    private Random rnd;

    public InsectMotionStrategy(float motionSpeed) {
        this.rnd = new Random();
        this.motionSpeed = motionSpeed;
        this.initialAcceleration = new Vector3(0,0,0);
    }

    private void getNewAcceleration(Light light) {
        light.acceleration.setToRandomDirection().nor().scl(this.motionSpeed * BASE_PROBABILITY * 0.25f);
    }

    public void initialise(Light light, float dt) {
        light.velocity = new Vector3().setToRandomDirection().nor().scl(this.motionSpeed);
        light.acceleration = this.initialAcceleration.cpy();
        this.getNewAcceleration(light);
    }

    @Override
    public void update(Light light, float dt) {
        float randomThreshold = BASE_PROBABILITY * this.motionSpeed * dt;
        if(this.rnd.nextFloat() < randomThreshold) {
            this.getNewAcceleration(light);

            if(light.velocity.len() > this.motionSpeed * 10) {
                light.velocity.nor().scl(this.motionSpeed);
            }
        }
    }

}
