package com.beachcafesoftware.bokeh;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

/*
 ** Guy Aaltonen
 */

public class BokehWallpaperGdx extends ApplicationAdapter {

    private static final int UPDATES_PER_SECOND = 60;
    private static final float TIME_PER_UPDATE = 1.0f / UPDATES_PER_SECOND;
    private static final long CAMERA_CHECK_INTERVAL = 50;

    public static float FADE_TIME = 3.0f;

    public static boolean preferenceChanged;
    private Preferences prefs;
    private static ArrayList<Formation> activeFormations;
    private static BokehRenderer ribbonRenderer;
    private static int numObjects[];
    private static FPSLogger fpsl;
    private String[] bgColors;
    private String[] fgColors;
    private Random rnd = new Random();
    private static String imageQuality;
    private static String filtering;
    private int[] diameter;
    private static int sleepDuration;
    private static String ribbonStyle;
    private static AppUsageLogger appLogger;
    private long lastRender = System.currentTimeMillis();
    private int[] motionSpeed;
    private static float speedSettingFloat;
    private static String lightType;
    private static float fpsLowerLimit;
    private static float fpsUpperLimit;
    private static float targetFps;
    private static int targetFpsInt;
    private int[] lifetime;
    private static float randomRangeNormalised;
    private int[] numberOfSides;
    private MotionStrategy motionStrategy;
    private float diameterSelected;
    private int lifetimeIndexSelected;
    private int[] lightBrightness;
    private int[] backgroundBrightness;
    private float backgroundBrightnessSelected;
    private int[] frequency;
    private int numberOfSidesSelected;
    private int[] motion;
    private int[] scale;
    private boolean scaleSelected;
    private float focalDistanceSelected;
    private int[] focalDistance;
    private int[] foregroundColorSetSize;
    private int foregroundColorSetSizeSelected;
    private ShaderManager shaderManager;
    private long lastTimeBucket;
    private int timeBucketDuration;
    private String[] fgColorsSelected;
    private float timeAccumulation;
    private int lastHeight;
    private int lastWidth;
    private long lastCameraCheck;


    @Override
    public void create() {
        // initialise managers
        this.shaderManager = new ShaderManager();

        // System.out.println("===== sm: create called");
        this.loadPreferences();

        fpsl = new FPSLogger();

        /*
        appLogger = new GdxWallpaperLogger();
        appLogger.setApplicationName("bokeh");
        appLogger.setUrl("http://!.!.!.!/logappusage.php");

        try {
            appLogger.log();
        } catch (ParametersNotSetException e) {
            e.printStackTrace();
        }
         */

    }

    /**
     * Loads preferences from disk, but doesn't do any randomisation or selection of them.
     * Only needs to be called at the start, or when preferences are changed by the user.
     */
    private void loadPreferences() {
        // System.out.println("===== sm: loadPreferences called");

        // scalar
        String imageQualityS;
        String filteringS;
        String fpsS;
        String randomnessS;
        String timeBucketDurationS;

        // multichoice
        String[] numObjectsS;
        String[] bgColorSchemeS;
        String[] fgColorSchemeS;
        String[] diameterS;
        String[] motionSpeedS;
        String[] focalDistanceS;
        String[] spriteLifeS;
        String[] lightBrightnessS;
        String[] backgroundBrightnessS;
        String[] frequencyS;
        String[] sidesS;
        String[] motionS;
        String[] scaleS;
        String[] colorSetSizeS;

        prefs = Gdx.app.getPreferences("cube2settings");

        // first, try to read from preference on disk

        // multichoice
        numObjectsS = this.getStringArrayFromSet("_numobjects", this.prefs);
        bgColorSchemeS = this.getStringArrayFromSet("_backgroundscheme", this.prefs);
        fgColorSchemeS = this.getStringArrayFromSet("_foregroundscheme", this.prefs);
        motionSpeedS = this.getStringArrayFromSet("_motionspeed", this.prefs);
        diameterS = this.getStringArrayFromSet("_diameter", this.prefs);
        spriteLifeS = this.getStringArrayFromSet("_spritelife", this.prefs);
        lightBrightnessS = this.getStringArrayFromSet("_lightbrightness", this.prefs);
        backgroundBrightnessS = this.getStringArrayFromSet("_backgroundbrightness", this.prefs);
        frequencyS = this.getStringArrayFromSet("_frequency", this.prefs);
        sidesS = this.getStringArrayFromSet("_sides", this.prefs);
        motionS = this.getStringArrayFromSet("_motion", this.prefs);
        scaleS = this.getStringArrayFromSet("_scalelights", this.prefs);
        focalDistanceS = this.getStringArrayFromSet("_focaldistance", this.prefs);
        colorSetSizeS = this.getStringArrayFromSet("_colorsetsize", this.prefs);

        // single choice
        imageQualityS = prefs.getString("_imagequality");
        filteringS = prefs.getString("_filtering");
        fpsS = prefs.getString("_fps");
        randomnessS = prefs.getString("_randomness");
        timeBucketDurationS = prefs.getString("_randomisetimer");

        // second, fill scalar values with defaults if the preference values were null.
        if (imageQualityS.equals("") == true) {
            prefs.putString("_imagequality", "medium");
        }

        if (filteringS.equals("") == true) {
            prefs.putString("_filtering", "bilinear");
        }

        if (fpsS.equals("") == true) {
            prefs.putString("_fps", "30");
        }

        if (randomnessS.equals("") == true) {
            prefs.putString("_randomness", "0.50");
        }

        if (timeBucketDurationS.equals("") == true) {
            prefs.putString("_randomisetimer", "30");
        }

        // save changes
        prefs.flush();

        this.ribbonStyle = prefs.getString("_ribbonstyle");

        // third, fill multichoice values with defaults if the preference values were null or empty.
        // null can be because the app was just installed, or data corruption.
        // empty can be because the user left everything checkbox empty.
        this.checkAndSetMultichoiceDefaults(numObjectsS, bgColorSchemeS, fgColorSchemeS, motionSpeedS, diameterS, spriteLifeS,
                lightBrightnessS, backgroundBrightnessS, frequencyS, sidesS, motionS, scaleS, focalDistanceS, colorSetSizeS);


        this.imageQuality = prefs.getString("_imagequality");
        this.filtering = prefs.getString("_filtering");
        this.targetFpsInt = Integer.parseInt(prefs.getString("_fps"));
        this.lightType = prefs.getString("_lighttype");
        this.randomRangeNormalised = Float.parseFloat(prefs.getString("_randomness"));
        this.timeBucketDuration = Integer.parseInt(prefs.getString("_randomisetimer"));
    }

    public void randomisePreferences() {
        // System.out.println("===== sm: randomisePreferences called");
        this.targetFps = (float)this.targetFpsInt;
        this.fpsLowerLimit = this.targetFps - 1.0f;
        this.fpsUpperLimit = this.targetFps + 1.0f;
        this.focalDistanceSelected = PreferenceConstants.focalDistances[Utilities.getRandomIndexFromIndices(focalDistance, this.rnd)];
        this.lifetimeIndexSelected = Utilities.getRandomIndexFromIndices(this.lifetime, this.rnd);
        this.backgroundBrightnessSelected = Utilities.getRandomValueFromRangeBucket(PreferenceConstants.backgroundBrightness, this.backgroundBrightness, true, this.rnd);
        float speed = Utilities.getRandomValueFromRangeBucket(PreferenceConstants.motionSpeeds, this.motionSpeed, false, this.rnd);
        this.motionStrategy = MotionStrategy.getStrategy(this.getRandomString(PreferenceConstants.motions, this.motion), Float.valueOf(speed));
        this.diameterSelected = Utilities.getRandomValueFromRangeBucket(PreferenceConstants.diameters, this.diameter, false, this.rnd);
        this.numberOfSidesSelected = (int) Utilities.getRandomValueFromRangeBucket(PreferenceConstants.sides, this.numberOfSides, false, this.rnd);
        this.scaleSelected = this.getRandomBoolean(PreferenceConstants.scales, this.scale);
        this.foregroundColorSetSizeSelected = (int) getRandomInteger(PreferenceConstants.foregroundColorSetSize, this.foregroundColorSetSize);
        this.sleepDuration = 1000 / this.targetFpsInt;

        ShaderProgram shaderProgramLightsFull = this.shaderManager.getShader("light", this.scaleSelected, false);
        ShaderProgram shaderProgramLightsFading = this.shaderManager.getShader("light", this.scaleSelected, true);
        ShaderProgram shaderProgramBackgroundFull = this.shaderManager.getShader("background", this.scaleSelected, false);
        ShaderProgram shaderProgramBackgroundFading = this.shaderManager.getShader("background", this.scaleSelected, true);

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        // turn the abstract setting of foreground colors to a calculated concrete set of colors.
        this.createForegroundColorSet();

         BokehWallpaperGdx.ribbonRenderer = new BokehRendererRetainedShader(
                 BokehWallpaperGdx.UPDATES_PER_SECOND,
                imageQuality,
                filtering,
                this.diameterSelected,
                lightType,
                 fgColorsSelected,
                bgColors,
                randomRangeNormalised,
                 this.lifetimeIndexSelected,
                this.lightBrightness,
                this.frequency,
                 this.numberOfSidesSelected,
                 this.motionStrategy,
                this.backgroundBrightnessSelected,
                 this.focalDistanceSelected,
                 shaderProgramLightsFull,
                 shaderProgramLightsFading,
                 shaderProgramBackgroundFull,
                 shaderProgramBackgroundFading
         );

        speedSettingFloat = 0.5f;
    }

    private int getRandomInteger(int[] allIntegers, int[] selectedIntegerIndices) {
        int bucketIndex = Utilities.getRandomIndexFromIndices(selectedIntegerIndices, this.rnd);
        int integerValue = allIntegers[bucketIndex];
        return integerValue;
    }

    private boolean getRandomBoolean(boolean[] allBooleans, int[] selectedBooleanIndices) {
        int bucketIndex = Utilities.getRandomIndexFromIndices(selectedBooleanIndices, this.rnd);
        boolean booleanValue = allBooleans[bucketIndex];
        return booleanValue;
    }

    /**
     * Returns random string from a string array, given a list of allowable indices.
     */
    private String getRandomString(String[] allStrings, int[] selectedStringIndices) {
        int bucketIndex = Utilities.getRandomIndexFromIndices(selectedStringIndices, this.rnd);
        String string = allStrings[bucketIndex];
        return string;
    }

    /**
     * To be clear, this doesn't actually save any defaults in the preferences, because libgdx can't write anything
     * except scalars (eg strings) to preferences. It just sets values to the variables that would have been loaded
     * from preferences if they were there.
     * Values (these defaults or user-selected) will be written to preferences the first time the user goes into
     * the Settings activity and clicks save. After this, defaults will never be needed again.
     * This method sets default for two cases:
     * 1. The values are null. This can be because the app was just installed, or data corruption, and possibly other reasons.
     * 2. The values are empty. This can be because the user left every setting completely unchecked.
     * Defaults must be put in because all settings need something selected, otherwise the wallpaper can't work.
     */
    private void checkAndSetMultichoiceDefaults(String[] numObjectsS, String[] bgColorSchemeS, String[] fgColorSchemeS,
                                                String[] motionSpeedS, String[] diameterS, String[] lifetimeS, String[] lightBrightnessS,
                                                String[] backgroundBrightnessS, String[] frequencyS, String[] sidesS, String[] motionS,
                                                String[] scaleS, String[] focalDistanceS, String[] foregroundColorSetSizeS) {

        if(foregroundColorSetSizeS == null || foregroundColorSetSizeS.length == 0) {
            // array of integers of focal distance indices
            this.foregroundColorSetSize = new int[] { 1 };
        } else {
            this.foregroundColorSetSize = this.convertStringArrayToIntArray(foregroundColorSetSizeS);
        }

        if(focalDistanceS == null || focalDistanceS.length == 0) {
            // array of integers of focal distance indices
            this.focalDistance = new int[] { 2 };
        } else {
            this.focalDistance = this.convertStringArrayToIntArray(focalDistanceS);
        }

        if(scaleS == null || scaleS.length == 0) {
            // array of integers of scale indices
            this.scale = new int[] { 0 };
        } else {
            this.scale = this.convertStringArrayToIntArray(scaleS);
        }

        if(motionS == null || motionS.length == 0) {
            // array of integers of motions indices
            this.motion = new int[] { 1 };
        } else {
            this.motion = this.convertStringArrayToIntArray(motionS);
        }

        if(sidesS == null || sidesS.length == 0) {
            // array of integers of sides indices
            this.numberOfSides = new int[] { 5 };
        } else {
            this.numberOfSides = this.convertStringArrayToIntArray(sidesS);
        }

        if(frequencyS == null || frequencyS.length == 0) {
            // array of integers of frequency indices
            this.frequency = new int[] { 2 };
        } else {
            this.frequency = this.convertStringArrayToIntArray(frequencyS);
        }

        if(backgroundBrightnessS == null || backgroundBrightnessS.length == 0) {
            // array of integers of background brightness indices
            this.backgroundBrightness = new int[] { 7 };
        } else {
            this.backgroundBrightness = this.convertStringArrayToIntArray(backgroundBrightnessS);
        }

        if(lightBrightnessS == null || lightBrightnessS.length == 0) {
            // array of integers of light brightness indices
            this.lightBrightness = new int[] { 7 };
        } else {
            this.lightBrightness = this.convertStringArrayToIntArray(lightBrightnessS);
        }

        if(lifetimeS == null || lifetimeS.length == 0) {
            // array of integers of lifetime indices
            this.lifetime = new int[] { 2 };
        } else {
            this.lifetime = this.convertStringArrayToIntArray(lifetimeS);
        }

        if(diameterS == null || diameterS.length == 0) {
            // array of integers of diameter indices
            this.diameter = new int[] { 2 };
        } else {
            this.diameter = this.convertStringArrayToIntArray(diameterS);
        }

        if(motionSpeedS == null || motionS.length == 0) {
            // array of integers of motion speed indices
            this.motionSpeed = new int[] { 2 };
        } else {
            this.motionSpeed = this.convertStringArrayToIntArray(motionSpeedS);
        }

        if(numObjectsS == null || numObjectsS.length == 0) {
            // array of integers of number of objects indices
            this.numObjects = new int[] { 3 };
        } else {
            // System.out.println("===== numobjects: " + numObjectsS + ", " + numObjectsS.length);
            this.numObjects = this.convertStringArrayToIntArray(numObjectsS);
        }

        if (bgColorSchemeS == null) {
            this.bgColors = new String[] { "redyellow", "bluemagenta" };
        } else {
            this.bgColors = bgColorSchemeS;
          //  System.out.println("===== first time, bgColorScheme has a value: " + bgColorSchemeS);
        }

        if (fgColorSchemeS == null) {
            this.fgColors = new String[] { "matchbg" };
        } else {
            this.fgColors = fgColorSchemeS;
           // System.out.println("===== first time, fgColorScheme has a value: " + fgColorSchemeS);
        }
    }

    /**
     * Utililty method to convert string array to int array
     */
    private int[] convertStringArrayToIntArray(String[] stringArray) {
        int[] intArray = new int[stringArray.length];

        for(int i = 0; i < stringArray.length; i++) {
            intArray[i] = Integer.valueOf(stringArray[i]);
        }

        return intArray;
    }

    /**
     * Turns the abstract setting of foreground colors into a concrete set of colors.
     */
    private void createForegroundColorSet() {
        // System.out.println("===== sm: createForegroundColorSet called");

        // second, choose one of the foreground colors randomly
        String randomForegroundColor = this.fgColors[ rnd.nextInt(this.fgColors.length) ];

        // white
        if(randomForegroundColor.equals("white") == true) {
            this.fgColorsSelected = new String[] { "white" } ;
        }

        // match background
        else if(randomForegroundColor.equals("matchbg") == true) {
            if(this.bgColors.length > 0) {
                this.fgColorsSelected = this.bgColors.clone();
            } else {
                // we can't choose the same set if they chose nothing, so just choose all colors for the foreground.
                this.fgColorsSelected = Color.SCHEMES_TYPE_4.clone();
            }
        }

        // opposite of background
        else if(randomForegroundColor.equals("oppositebg") == true) {
            List<String> foregroundColorsNext = new ArrayList<String>(Color.SCHEMES_TYPE_4.length);
            List<String> schemesList = Arrays.asList(Color.SCHEMES_TYPE_4);
            List<String> bgColorsList = Arrays.asList(this.bgColors);

            int numberOfForegroundColors = Color.SCHEMES_TYPE_4.length - this.bgColors.length;

            if(numberOfForegroundColors > 0) {
                for(int i = 0; i < schemesList.size(); i++) {
                    String schemesColor = schemesList.get(i);

                    if(bgColorsList.contains(schemesColor) == false) {
                        System.out.println("===== bgColorsList doesn't contain " + schemesColor);
                        foregroundColorsNext.add(schemesColor);
                    } else {
                        System.out.println("===== bgColorsList does contain " + schemesColor);
                    }
                }
                this.fgColorsSelected = (String[]) foregroundColorsNext.toArray(new String[foregroundColorsNext.size()]);
            } else {
                // we can't choose the exclusive set if they chose everything, so just choose all colors for the foreground.
                this.fgColorsSelected = Color.SCHEMES_TYPE_4.clone();
            }
        }

        // any
        else if(randomForegroundColor.equals("any") == true) {
            this.fgColorsSelected = Color.SCHEMES_TYPE_4.clone();
        }

        // first, because the foreground color set is restricted by the color set size, we cut it down.
        List<String> foregroundColorsAsList = Arrays.asList(this.fgColorsSelected);
        Collections.shuffle(foregroundColorsAsList);
        this.fgColorsSelected = (String[])foregroundColorsAsList.toArray(new String[foregroundColorsAsList.size()]);
        int elementsToCopy = Math.min(this.foregroundColorSetSizeSelected, foregroundColorsAsList.size());
        this.fgColorsSelected = Arrays.copyOfRange(this.fgColorsSelected, 0, elementsToCopy);
    }

    private String[] listToArray(ArrayList<String> bgcolors) {
        String[] strings = new String[bgcolors.size()];

        for(int i = 0; i < bgcolors.size(); i++) {
            strings[i] = bgcolors.get(i);
        }

        return strings;
    }

    private String printStringArrayAsString(String[] bgColorSchemeS) {
        String string = "[";
        for(int i = 0; i < bgColorSchemeS.length; i++) {
            string += bgColorSchemeS[i] + ",";
        }
        string += "]";
        return string;
    }


    private String[] getStringArrayFromSet(String preferencesKey, Preferences prefs) {

        Map<String, ?> preferences = prefs.get();

        if(preferences.get(preferencesKey) == null) {
            return null;
        }

        HashSet hashSet = (HashSet)preferences.get(preferencesKey);
        Object[] objectArray = (Object[])hashSet.toArray();

        String[] stringArray = new String[objectArray.length];
        for(int i = 0; i < objectArray.length; i++) {
            stringArray[i] = (String)objectArray[i];
        }

        return stringArray;
    }



    public void initialiseFormations() {
        // System.out.println("===== sm: initialiseFormations called");

        // the only time this is ever reached, we are starting with no activeFormations except the default no-formation
        activeFormations = new ArrayList<Formation>();
        Formation noFormation = new NoFormation();
        noFormation.setSpeed(speedSettingFloat);
        activeFormations.add(noFormation);
        ArrayList<Ribbon> noFormationRibbons = new ArrayList<Ribbon>();

        // create all the ribbons.
        int numberOfRibbons = (int)Utilities.getRandomValueFromRangeBucket(PreferenceConstants.numberOfLights, this.numObjects, false, this.rnd);

        // System.out.println("===== nr: " + numberOfRibbons);

        for (int i = 0; i < numberOfRibbons; i++) {
            Vector3 velocity = new Vector3();
            float speed = Ribbon.MIN_SPEED + rnd.nextFloat() * (Ribbon.MAX_SPEED - Ribbon.MIN_SPEED);
            velocity.z = speed;
            Vector3 position = new Vector3();
            position.x = Ribbon.BOUNDARY_LEFT + rnd.nextFloat() * (Ribbon.BOUNDARY_RIGHT - Ribbon.BOUNDARY_LEFT);
            position.y = Ribbon.BOUNDARY_TOP + rnd.nextFloat() * (Ribbon.BOUNDARY_BOTTOM - Ribbon.BOUNDARY_TOP);
            position.z = Ribbon.BOUNDARY_FRONT;
            //Ribbon r1 = new Ribbon(position, velocity, this.diameterSelected, fgColors, speedSettingFloat, targetFps);
            Ribbon r1 = new Ribbon(position, velocity, this.diameterSelected, fgColors, speedSettingFloat, UPDATES_PER_SECOND);

            noFormationRibbons.add(r1);
        }
        noFormation.addRibbons(noFormationRibbons, speedSettingFloat);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
    }

    private void checkCamera() {

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        if(
                this.lastWidth != width ||
                this.lastHeight != height
        ) {
            System.out.println("===== calling resetcamera");
                BokehWallpaperGdx.ribbonRenderer.resetCamera();
                this.lastWidth = width;
                this.lastHeight = height;
        }
    }

    @Override
    public void render() {

        // TODO: improve the frame limiter. just do it the way games do, if possible.
        // TODO: run as many updates as we should do, based on the current time.
        if (sleepDuration != 0) {
            try {
                Thread.sleep(sleepDuration);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (Gdx.graphics.getRawDeltaTime() > (1.0f / fpsLowerLimit)) {
            sleepDuration -= 1;
            if (sleepDuration < 0) sleepDuration = 0;
        } else if (Gdx.graphics.getRawDeltaTime() < (1.0f / fpsUpperLimit)) {
            sleepDuration += 1;
        }

        if(BokehWallpaperGdx.preferenceChanged == true) {
            BokehWallpaperGdx.preferenceChanged = false;
            this.loadPreferences();
            this.randomisePreferences();
            this.initialiseFormations();
        }

        long now = System.currentTimeMillis();
        long timeSinceLastRenderLong = now - lastRender;
        float timeSinceLastRenderFloat = timeSinceLastRenderLong / 1000.0f;
        this.timeAccumulation += timeSinceLastRenderFloat;

        // number of updates we need to do is the whole number of this division, discarding the decimal.
        int numberOfUpdatesToDo = (int) (this.timeAccumulation / BokehWallpaperGdx.TIME_PER_UPDATE);

      //  System.out.println("===== ft: " + numberOfUpdatesToDo + ", " + timeSinceLastRenderFloat + ", " + TIME_PER_UPDATE);

        // decrement time accumulation by the whole number of updates we are going to do.
        this.timeAccumulation -= numberOfUpdatesToDo * BokehWallpaperGdx.TIME_PER_UPDATE;

        float brightness = getFadeBrightness();
        BokehWallpaperGdx.ribbonRenderer.updateRibbons(numberOfUpdatesToDo, activeFormations);
        BokehWallpaperGdx.ribbonRenderer.updateLights1(numberOfUpdatesToDo);

        BokehWallpaperGdx.ribbonRenderer.renderLights(brightness);

        fpsl.log();

        if(now > this.lastCameraCheck + BokehWallpaperGdx.CAMERA_CHECK_INTERVAL) {
            this.checkCamera();
            this.lastCameraCheck = now;
        }

        lastRender = now;

    }

    /**
     * Returns brightness of scene as a float 0 to 1, based on the current time and when the scene started.
     */
    private float getFadeBrightness() {
        long timeLong = System.currentTimeMillis() / 1000;
        int timeBucket = ((int)timeLong / this.timeBucketDuration);
        double timeFloat = (double)System.currentTimeMillis() / 1000.0;

        if (timeBucket != this.lastTimeBucket) {
            this.randomisePreferences();
            this.initialiseFormations();
            this.lastTimeBucket = timeBucket;
        }

        long bucketBeginning = timeBucket * this.timeBucketDuration;
        double timeToBucketBeginning = timeFloat - bucketBeginning;
        double timeToBucketEnd = (bucketBeginning + this.timeBucketDuration) - timeFloat;
        double timeToBucketBoundary = Math.max(Math.min(timeToBucketBeginning, timeToBucketEnd), 0.0f);
        float brightness = (float)Math.min(timeToBucketBoundary / BokehWallpaperGdx.FADE_TIME, 1.0f);
        return brightness;
    }
}
