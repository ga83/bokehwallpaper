package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.math.Vector3;

public class TowardsStraightFixedMotionStrategy extends MotionStrategy {

    private final float motionSpeed;

    public TowardsStraightFixedMotionStrategy(float motionSpeed) {
        this.motionSpeed = motionSpeed;
    }

    public void initialise(Light light, float dt) {
        light.velocity = new Vector3(0, 0, 0 - motionSpeed);
        light.acceleration = new Vector3(0, 0, 0);
    }

    @Override
    public void update(Light light, float dt) {

    }
}
