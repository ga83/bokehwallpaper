package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

public class BokehRendererRetainedShader implements BokehRenderer {

    private static final int COLOR_TABLE_SIZE = 100;
    private static final float BACKGROUND_SQUARE_SIDE_LENGTH = 2000.0f;
    private static final Vector3 CENTRE_VERTEX = new Vector3(0.0f, 0.0f, 0.0f);
    private static final float BACKGROUND_DISTANCE = 1000.0f;
    private static int LIGHT_RADIUS;
    private final int[] lightBrightness;
    private final float focalDistance;

    private static final int PARTICLES_LENGTH = 5000;

    private final float randomAngleConstant;
    private final float angleDelta;

    private KirraArrayList<Light> lights = new KirraArrayList<Light>(PARTICLES_LENGTH);

    private float updatesPerSecond;

    // vertex attribute constants for lights
    private static final int NUMBER_OF_POSITION_COMPONENTS = 3;	// xyz
    private static final int NUMBER_OF_COLOR_COMPONENTS = 4;		// rgb
    private static final int NUMBER_OF_DISTANCE_COMPONENTS = 1;
    public static final int NUMBER_OF_COMPONENTS = NUMBER_OF_POSITION_COMPONENTS + NUMBER_OF_COLOR_COMPONENTS + NUMBER_OF_DISTANCE_COMPONENTS;

    // vertex attribute constants for background
    private static final int NUMBER_OF_POSITION_COMPONENTS_BACKGROUND = 3;	// xyz
    private static final int NUMBER_OF_UV_COMPONENTS_BACKGROUND = 2;		// uv
    public static final int NUMBER_OF_COMPONENTS_BACKGROUND = NUMBER_OF_POSITION_COMPONENTS_BACKGROUND + NUMBER_OF_UV_COMPONENTS_BACKGROUND;    // xyz

    // shaders and related fields
    //full
    private ShaderProgram shaderProgramLightsFull;
    private ShaderProgram shaderProgramBackgroundFull;
    private int[] attributeBackgroundFullLocations;
    private int[] attributeLightsFullLocations;
    private int u_projModelViewBackgroundFullLocation;
    private int brightnessBackgroundFullLocation;
    private int lightPosFullLocation;
    private int u_projModelViewFullLocation;
    private int focalDistanceFullLocation;
    private int brightnessFullLocation;

    // fading
    private ShaderProgram shaderProgramBackgroundFading;
    private ShaderProgram shaderProgramLightsFading;
    private int[] attributeBackgroundFadingLocations;
    private int[] attributeLightsFadingLocations;
    private int u_projModelViewBackgroundFadingLocation;
    private int brightnessBackgroundFadingLocation;
    private int lightPosFadingLocation;
    private int u_projModelViewFadingLocation;
    private int focalDistanceFadingLocation;
    private int brightnessFadingLocation;
    private int fadeBrightnessLightFadingLocation;
    private int fadeBrightnessBackgroundFadingLocation;

    PerspectiveCamera cam;

    int numSides;

    private static float MAX_OFFSET_FACTOR = 4.0f;  // multiplier of diameter

    private Random rnd = new Random();

    private static int NUMBER_OF_UPDATES_BETWEEN_ADD = 15;
    private float[][] backgroundColorTable;
    private float[][] foregroundColorTable;

    private float randomRangeNormalised;

    private int[] motionSpeed;
    private int lifetimeIndex;
    private int[] frequency;
    private float backgroundBrightnessSelected;
    private MotionStrategy motionStrategy;

    // background
    private Background background;
    private float backgroundBrightness;

    // fixed timestep logic
    private int numberOfUpdatesSinceAdd;


    /**
     * Draws in retained mode.
     */
    @Override
    public void updateRibbons(int numberOfUpdatesToDo, ArrayList<Formation> formations) {


//         System.out.println("===== gg cam update");
        cam.update();

        // fixed timestep, update number of times times.
        for(int step = 0; step < numberOfUpdatesToDo; step++) {

            boolean addLight = this.numberOfUpdatesSinceAdd == BokehRendererRetainedShader.NUMBER_OF_UPDATES_BETWEEN_ADD;
            // update
            for (int i = 0; i < formations.size(); i++) {
                Formation formation = formations.get(i);
                formation.updateAllRibbons(this.updatesPerSecond);
                ArrayList<Ribbon> formationRibbons = formation.getRibbons();

                // if time to add a new light, add one
                if (addLight == true) {
                    for (int j = 0; j < formationRibbons.size(); j++) {
                        this.createLight(formationRibbons.get(j));
                    }
                }
            }

            if (addLight == true) {
                this.numberOfUpdatesSinceAdd = 0;
            } else {
                this.numberOfUpdatesSinceAdd++;
            }
        }
    }


    /**
     * Creates a new light riding on the given Ribbon.
     * @param ribbon
     */
    public void createLight(Ribbon ribbon) {

        float displacement = ribbon.diameter * MAX_OFFSET_FACTOR * rnd.nextFloat();

        if (ribbon.position.z - displacement < 0) {
            return;
        }   // z culling

        Vector3 randomOffset = new Vector3().setToRandomDirection().scl(displacement);
        Vector3 randomisedPosition = ribbon.position.cpy().add(randomOffset);

        Light light = new Light(this.motionStrategy);
        light.position = randomisedPosition;
        light.velocity = new Vector3();

        this.motionStrategy.initialise(light, this.updatesPerSecond);

        float lifetimeMinimum = PreferenceConstants.lifetimes[this.lifetimeIndex][0];
        float lifetimeMaximum = PreferenceConstants.lifetimes[this.lifetimeIndex][1];
        float lifetimeMedian = (lifetimeMinimum + lifetimeMaximum) / 2.0f;

        float life = Utilities.getRandomisedValue(lifetimeMedian, randomRangeNormalised, lifetimeMinimum, lifetimeMaximum, this.rnd);
        light.life = life;
        light.initialLife = life;

        light.brightnessInitial = Utilities.getRandomValueFromRangeBucket(PreferenceConstants.lightBrightness, this.lightBrightness, false, this.rnd);

        light.periodicFrequencies = new float[2];


        light.periodicFrequencies[0] = Utilities.getRandomValueFromRangeBucket(PreferenceConstants.frequencies, this.frequency, false, this.rnd);
        light.periodicFrequencies[1] = Utilities.getRandomValueFromRangeBucket(PreferenceConstants.frequencies, this.frequency, false, this.rnd);

        float[] color = foregroundColorTable[rnd.nextInt(foregroundColorTable.length)];

        light.r = color[0];
        light.g = color[1];
        light.b = color[2];

        this.initialiseLight(light);
        this.lights.add(light);

    }

    /**
     * Update lights.
     */
    @Override
    public void updateLights1(int numberOfUpdatesToDo) {

        // System.out.println("===== nl: " + this.lights.size());

        for(int step = 0; step < numberOfUpdatesToDo; step++) {

            // update lights
            for (int i = 0; i < lights.size(); i++) {
                Light light = lights.get(i);
                light.updateMotion(this.updatesPerSecond);
                light.updateVelocity();
                light.updatePosition(this.updatesPerSecond);
                light.updateLife(this.updatesPerSecond);

                // remove light if it's out of life.
                if (light.life < 0.0f) {
                    // dispose vbo data before removing light
                    lights.get(i).dispose();
                    lights.remove(i);
                    // wind back loop because we deleted current light.
                    i--;

                    continue;
                }
            }
        }
        this.updateLights2();
    }

    /**
     * High-level rendering method which calculates data related to drawing lights. eg the current brightness, culled status, etc.
     */
    public void updateLights2() {

        Gdx.gl.glClearColor(0, 0, 0, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        int drawing = 0;
        // update lights
        for(int i = 0; i < lights.size(); i++) {
            Light light = lights.get(i);

            // don't draw it if it's behind the camera, outside frustum, or dead

            // z culling, behind camera
            if(light.position.z < 0) {
                light.renderLight = false;
                continue;
            }

            // light outside frustum
            else if(cam.frustum.sphereInFrustum(light.position,LIGHT_RADIUS * 1.0f) == false) {
                light.renderLight = false;
                continue;
            }

            drawing++;
            light.renderLight = true;

            // calculate brightness based on summed periodics.
            double period = light.initialLife * 2;

            // the envelope is the sinusoidal overarching amplitude which starts at 0, goes to 1, and ends back at 0.
            double envelope = Math.sin((light.life / period) * (2 * Math.PI));

            float sinesSum = 0.0f;
            sinesSum += 1.0 * Math.cos(light.life * light.periodicFrequencies[0]);  // wave 1 contributes up to 0.4
            sinesSum += 1.0 * Math.cos(light.life * light.periodicFrequencies[1]);  // wave 2 contributes up to 0.4
            sinesSum += 2.0;     // lift bottom to 0.0 by moving centre to 0.5 so amplitude can't be negative.
            sinesSum *= 0.25f;

            float brightness = clamp((float) (light.brightnessInitial * envelope * sinesSum));

            light.brightnessRuntime = brightness;
        }
//        System.out.println("===== drawing: " + drawing);

    }

    @Override
    public void renderLights(float sceneBrightness) {
        // choose rendering path based on whether scene is fading or not.
        boolean full = sceneBrightness == 1.0f;
        if(full == true) {
  //          System.out.println("===== fd: not fading, " + sceneBrightness);
            this.renderBackgroundFull(this.background);
            this.renderLightsFull(this.lights);
        } else {
    //        System.out.println("===== fd: yes fading " + sceneBrightness);
            this.renderBackgroundFading(this.background, sceneBrightness);
            this.renderLightsFading(this.lights, sceneBrightness);
        }
    }

    /**
     * Calculates all the vertices of the light, relative to the origin.
     * In this retained renderer, this should only be called once upon light creation.
     * Also creates the vertex buffer object and uploads it to the GPU.
     */
    private void initialiseLight(Light light) {
        light.allocateVertices(this.numSides);
        KirraArrayList<Triangle> triangles = new KirraArrayList<Triangle>(this.numSides);

        for(int i = 0; i < this.numSides; i++) {

            float x;
            float y;

            // TODO: remove
            //Vector3 vertex1 = new Vector3(0,0,0);

            x = (float) (Math.cos(Math.toRadians(i * this.angleDelta + this.randomAngleConstant)) * LIGHT_RADIUS);
            y = (float) (Math.sin(Math.toRadians(i * this.angleDelta + this.randomAngleConstant)) * LIGHT_RADIUS);
            Vector3 vertex2 = new Vector3(0,0,0).add(x, y, 0);

            x = (float) (Math.cos(Math.toRadians((i + 1) * this.angleDelta + this.randomAngleConstant)) * LIGHT_RADIUS);
            y = (float) (Math.sin(Math.toRadians((i + 1) * this.angleDelta + this.randomAngleConstant)) * LIGHT_RADIUS);
            Vector3 vertex3 = new Vector3(0,0,0).add(x, y, 0);

            Triangle triangle = new Triangle(BokehRendererRetainedShader.CENTRE_VERTEX, vertex2, vertex3, new com.badlogic.gdx.graphics.Color(light.r, light.g, light.b, 1.0f));
            triangles.add(triangle);
        }

        VboDataRetained vboDataRetained = new VboDataRetained(triangles);
        light.vboDataRetained = vboDataRetained;
    }

    private float clamp(float v) {
        if (v > 1.0) {
            return 1.0f;
        } else if (v < 0.0) {
            return 0.0f;
        } else {
            return v;
        }
    }

    /**
     * Renders the background vbo, without fading.
     */
    private void renderBackgroundFading(Background background, float sceneBrightness) {
        this.shaderProgramBackgroundFading.begin();

        // set uniforms
        this.shaderProgramBackgroundFading.setUniformMatrix(this.u_projModelViewBackgroundFadingLocation, this.cam.combined);
        this.shaderProgramBackgroundFading.setUniformf(this.brightnessBackgroundFadingLocation, this.backgroundBrightness);
        this.shaderProgramBackgroundFading.setUniformf(this.fadeBrightnessBackgroundFadingLocation, sceneBrightness);

        // set texture uniform
        background.vboDataRetained.texture.bind();
        VertexBufferObject vertexBufferObject = background.vboDataRetained.vertexBufferObject;
        vertexBufferObject.bind(this.shaderProgramBackgroundFading, this.attributeBackgroundFadingLocations);
        vertexBufferObject.bind(this.shaderProgramBackgroundFading);
        Gdx.gl20.glDrawArrays(GL20.GL_TRIANGLES, 0, vertexBufferObject.getNumVertices());
        shaderProgramBackgroundFading.end();
    }

    /**
     * Renders the background vbo, without fading.
     */
    private void renderBackgroundFull(Background background) {
        this.shaderProgramBackgroundFull.begin();

        // set uniforms
        this.shaderProgramBackgroundFull.setUniformMatrix(this.u_projModelViewBackgroundFullLocation, this.cam.combined);
        this.shaderProgramBackgroundFull.setUniformf(this.brightnessBackgroundFullLocation, this.backgroundBrightness);

        // set texture uniform
        background.vboDataRetained.texture.bind();
        VertexBufferObject vertexBufferObject = background.vboDataRetained.vertexBufferObject;
        vertexBufferObject.bind(this.shaderProgramBackgroundFull, this.attributeBackgroundFullLocations);

        vertexBufferObject.bind(this.shaderProgramBackgroundFull);
        Gdx.gl20.glDrawArrays(GL20.GL_TRIANGLES, 0, vertexBufferObject.getNumVertices());

        shaderProgramBackgroundFull.end();
    }

    /**
     * Render just the lights, fading.
     */
    public void renderLightsFading(KirraArrayList<Light> lights, float sceneBrightness) {
        this.shaderProgramLightsFading.begin();

        // set temporary float array
        float[] lightPositionArray = new float[3];

        // set light-independent uniforms
        this.shaderProgramLightsFading.setUniformMatrix(this.u_projModelViewFadingLocation, this.cam.combined);
        this.shaderProgramLightsFading.setUniformf(this.focalDistanceFadingLocation, this.focalDistance);
        this.shaderProgramLightsFading.setUniformf(this.fadeBrightnessLightFadingLocation, sceneBrightness);

        for(int i = 0; i < lights.size(); i++) {
            Light light = lights.get(i);

            if(light.renderLight == false) {
                continue;
            }

            VertexBufferObject vertexBufferObject = light.vboDataRetained.vertexBufferObject;

            Vector3 lightPosition = light.position;
            lightPositionArray[0] = lightPosition.x;
            lightPositionArray[1] = lightPosition.y;
            lightPositionArray[2] = lightPosition.z;

            // set light-dependent uniforms
            this.shaderProgramLightsFading.setUniform3fv(this.lightPosFadingLocation, lightPositionArray, 0, 3);
            this.shaderProgramLightsFading.setUniformf(this.brightnessFadingLocation, light.brightnessRuntime);

            // TODO: faster, since we saved the attribute locations. however, bind() is still costly, so look into instancing. requires openGL 3.0.
            vertexBufferObject.bind(this.shaderProgramLightsFading, this.attributeLightsFadingLocations);

            Gdx.gl20.glDrawArrays(GL20.GL_TRIANGLES, 0, vertexBufferObject.getNumVertices());
        }

        shaderProgramLightsFading.end();
    }

    /**
     * Render just the lights, not fading.
     */
    public void renderLightsFull(KirraArrayList<Light> lights) {

        this.shaderProgramLightsFull.begin();

/*
        // testing and trying to enable opengl 3.0.
        boolean gl1 = Gdx.gl   != null;
        boolean gl2 = Gdx.gl20 != null;
        boolean gl3 = Gdx.gl30 != null;

        boolean gl2x = Gdx.graphics.getGL20() != null;
        boolean gl3x = Gdx.graphics.getGL30() != null;

        String glv2 = Gdx.gl20.glGetString(GL20.GL_VERSION);
       // String glv3 = Gdx.gl30.glGetString(GL30.GL_VERSION);

        boolean gl3av = Gdx.graphics.isGL30Available();

        String glversion = gl1 + ", " + gl2 + ", " + gl3 + ", " + gl2x + ", " + gl3x + ", " + glv2 + ", " + gl3av;
        System.out.println("===== gl version: " + glversion);
*/

        // set temporary float array
        float[] lightPositionArray = new float[3];

        // set light-independent uniforms
        this.shaderProgramLightsFull.setUniformMatrix(this.u_projModelViewFullLocation, this.cam.combined);
        this.shaderProgramLightsFull.setUniformf(this.focalDistanceFullLocation, this.focalDistance);

        for(int i = 0; i < lights.size(); i++) {
            Light light = lights.get(i);

            if(light.renderLight == false) {
                continue;
            }

           // System.out.println("===== rd: rendering light");

            VertexBufferObject vertexBufferObject = light.vboDataRetained.vertexBufferObject;

            Vector3 lightPosition = light.position;
            lightPositionArray[0] = lightPosition.x;
            lightPositionArray[1] = lightPosition.y;
            lightPositionArray[2] = lightPosition.z;

            // set light-dependent uniforms
            this.shaderProgramLightsFull.setUniform3fv(this.lightPosFullLocation, lightPositionArray, 0, 3);
            this.shaderProgramLightsFull.setUniformf(this.brightnessFullLocation, light.brightnessRuntime);

            // TODO: faster, since we saved the attribute locations. however, bind() is still costly, so look into instancing. requires openGL 3.0.
           vertexBufferObject.bind(this.shaderProgramLightsFull, this.attributeLightsFullLocations);

            Gdx.gl20.glDrawArrays(GL20.GL_TRIANGLES, 0, vertexBufferObject.getNumVertices());

         //   Gdx.gl30.glDrawArrays(GL30.GL_TRIANGLES, 0, vertexBufferObject.getNumVertices());

        }

        this.shaderProgramLightsFull.end();
    }


    public BokehRendererRetainedShader(
            int updatesPerSecond,
            String imageQuality,
            String filtering,
            float diameter,
            String lightType,
            String[] fgColors,
            String[] bgColors,
            float randomRangeNormalised,
            int lifetimeIndex,
            int[] lightBrightness,
            int[] frequency,
            int numSides,
            MotionStrategy motionStrategy,
            float backgroundBrightnessSelected,
            float focalDistance,
            ShaderProgram shaderProgramLightsFull,
            ShaderProgram shaderProgramLightsFading,
            ShaderProgram shaderProgramBackgroundFull,
            ShaderProgram shaderProgramBackgroundFading
            ) {

        this.setCamera();
        this.cam.update();

        System.out.println("===== gg camera initialised");

//        System.out.println("===== gg cam update");

        this.shaderProgramLightsFull = shaderProgramLightsFull;
        this.shaderProgramLightsFading = shaderProgramLightsFading;
        this.shaderProgramBackgroundFull = shaderProgramBackgroundFull;
        this.shaderProgramBackgroundFading = shaderProgramBackgroundFading;

        this.numSides = numSides;

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);
        Gdx.gl20.glDepthMask(false);
        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE);

        // from BokehRenderer
        LIGHT_RADIUS = (int) (diameter * 1.0);

        // this.numberOfUpdatesBetweenAdd = 15;

        this.updatesPerSecond = 1.0f / updatesPerSecond;

        this.randomRangeNormalised = randomRangeNormalised;
        this.lifetimeIndex = lifetimeIndex;
        this.lightBrightness = lightBrightness;
        this.frequency = frequency;
        this.motionStrategy = motionStrategy;
        this.backgroundBrightnessSelected = backgroundBrightnessSelected;
        this.focalDistance = focalDistance;

        // fill color tables
        this.foregroundColorTable = new float[COLOR_TABLE_SIZE][3];
        this.backgroundColorTable = new float[COLOR_TABLE_SIZE][3];
        this.fillColorTable(this.foregroundColorTable, fgColors);

        if(bgColors.length > 0) {
            this.fillColorTable(this.backgroundColorTable, bgColors);
            this.generateTexture(bgColors);
        }

        this.initialiseShader();

        this.angleDelta = 360.0f / this.numSides;  // number of degrees between each side
        this.randomAngleConstant = this.rnd.nextFloat() * this.angleDelta;
    }


    private void fillColorTable(float[][] colorTable, String[] colors) {
            for (int i = 0; i < colorTable.length; i++) {
                float[] color = Color.getRandomColorFromScheme(colors, false, i % 2 == 0);
                if (color == null) {
                    System.out.println("===== color is null at " + i);
                }
                colorTable[i] = color;
            }
    }
    
    /**
     * Generates a texture from noise.
     */
    private void generateTexture(String[] bgColors) {

        float[] colorLow = Color.getRandomColorFromScheme(bgColors, true, true);
        float[] colorHigh = Color.getRandomColorFromScheme(bgColors, true, true);

        float aspectRatio = Gdx.graphics.getWidth() / (float)Gdx.graphics.getHeight();

        int pixmapWidth = (int) (Gdx.graphics.getWidth() / 64f);
        int pixmapHeight = (int) (Gdx.graphics.getHeight() / 64f);

        Pixmap gradientPixmap = new Pixmap(pixmapWidth, pixmapHeight, Pixmap.Format.RGBA8888);
        gradientPixmap.setBlending(Pixmap.Blending.None);

        float lowest = 100;
        float highest = -100;

        float hueOffsetDelta = 0.2f / gradientPixmap.getHeight();
        float hueOffset;
        float hueYOffsetInitial = rnd.nextFloat() * 100.0f;
        float hueYOffset;
        float hueXOffset = rnd.nextFloat() * 100.0f;

        float lumYOffsetInitial = rnd.nextFloat() * 100.0f;
        float lumYOffset;
        float lumXOffset = rnd.nextFloat() * 100.0f;

        float satYOffsetInitial = rnd.nextFloat() * 100.0f;
        float satYOffset;
        float satXOffset = rnd.nextFloat() * 100.0f;


        float dxHue = 2.0f / pixmapWidth;
        float dyHue = (2.0f * (1f / aspectRatio)) / pixmapHeight;

        float dxLum = 3.0f / pixmapWidth;
        float dyLum = (3.0f * (1f / aspectRatio)) / pixmapHeight;

        float dxSat = 4.0f / pixmapWidth;
        float dySat = (3.0f * (1f / aspectRatio)) / pixmapHeight;

        this.backgroundBrightness = this.backgroundBrightnessSelected;

        // nested loop to generate every pixel of the pixmap (texture to be)
        for(int i = 0; i < gradientPixmap.getWidth(); i++) {
            hueOffset = -0.1f;
            hueYOffset = hueYOffsetInitial;
            lumYOffset = lumYOffsetInitial;
            satYOffset = satYOffsetInitial;

            for(int j = 0; j < gradientPixmap.getHeight(); j++) {

                // get noise value for hue
                float noiseValueHue = (float) ((ImprovedNoise.noise(hueXOffset, hueYOffset, 0f) * 0.5f) + 0.5f);
                noiseValueHue = this.wrap(noiseValueHue);
                // System.out.println("===== hue: " + noiseValueHue);

                // get noise value for lum
                 // float noiseValueLum = (float) ((ImprovedNoise.noise(lumXOffset, lumYOffset, 0f) * 0.5f) + 0.5f);
                 float noiseValueLum = (float) ((ImprovedNoise.noise(lumXOffset, lumYOffset, 0f) * 0.33f) + 0.66f);

                // get noise value for lum
                float noiseValueSat = (float) ((ImprovedNoise.noise(satXOffset, satYOffset, 0f) * 0.5f) + 0.5f);

                lowest = Math.min(lowest, noiseValueHue);
                highest = Math.max(highest, noiseValueHue);

                hueOffset += hueOffsetDelta;

                float colorLowShifted[] = Color.hslToRgb(colorLow[0] + hueOffset, colorLow[1] * noiseValueSat, colorLow[2]);
                float colorHighShifted[] = Color.hslToRgb(colorHigh[0] + hueOffset, colorHigh[1] * noiseValueSat, colorHigh[2]);

                // now that we have rgb, lerp the rgb values between lowest and highest.
                float[] color2 = Color.lerpRgb(colorLowShifted, colorHighShifted, noiseValueHue);

                color2[0] *= noiseValueLum;
                color2[1] *= noiseValueLum;
                color2[2] *= noiseValueLum;

                gradientPixmap.setColor(color2[0], color2[1], color2[2], 1f);
                gradientPixmap.drawPixel(i, j);

                hueYOffset += dyHue;
                lumYOffset += dyLum;
                satYOffset += dySat;
            }

            hueXOffset += dxHue;
            lumXOffset += dxLum;
            satXOffset += dxSat;
        }

        // dispose previous backround texture
        if(this.background != null) {
            this.background.dispose();
        }

        // get a good sized background... it has to be just large enough to be outside each corner.
        float backgroundSideLength = this.getBackgroundSideLength(BACKGROUND_DISTANCE);

        System.out.println("===== gg bgsl: " + backgroundSideLength);

        // generate new background texture
        this.background = new Background(gradientPixmap, GeometryOperations.getBackgroundTriangles(backgroundSideLength, BACKGROUND_DISTANCE));

        this.resizeBackground();

    }

    /**
     * This should be called whenever the screen's dimensions change in any way. Example, orientation change. It rebuilds the background geometry.
     */
    public void resetCamera() {
        this.setCamera();

       if(this.background != null) {
            this.resizeBackground();
        }
    }

    private void setCamera() {
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        float yFieldOfView;
        if(height > width) {
            yFieldOfView = 90.0f;
        }
        else if(width > height) {
            yFieldOfView = 60.0f;
        }
        else {
            yFieldOfView = 75.0f;
        }

        System.out.println("===== yfov: " + yFieldOfView + ", " + width + ", " + height);

        this.cam = new PerspectiveCamera(yFieldOfView, width, height);
        this.cam.position.x = 0;
        this.cam.position.y = 0;
        this.cam.position.z = 0;
        this.cam.lookAt(0, 0, 1f);
        this.cam.near = 0.1f;   // must be greater than 0 otherwise depth test fails frequently
        this.cam.far = 6000;
        this.cam.update();
    }

    public void resizeBackground() {
        System.out.println("===== reset background");
        this.cam.viewportWidth = Gdx.graphics.getWidth();
        this.cam.viewportHeight = Gdx.graphics.getHeight();
        this.cam.update();
        float backgroundSideLength = this.getBackgroundSideLength(BACKGROUND_DISTANCE);

        // dispose geometry but not the texture.
        this.background.vboDataRetained.dispose(false,"");

        // recreate the geometry only, don't run the background constructor.
        this.background.vboDataRetained.createVboData(GeometryOperations.getBackgroundTriangles(backgroundSideLength, BACKGROUND_DISTANCE));
    }

    private float getBackgroundSideLength(float backgroundDistance) {
        float sideLength = 100.0f;
        boolean bothPointsOutside = false;
        Vector3 pointX = new Vector3();
        Vector3 pointY = new Vector3();

        int i = 0;
        while(bothPointsOutside == false) {
            // we only test one corner's point, which is fine because it is symmetrical around the camera.
            pointX.set(sideLength / 2.0f, 0.0f, backgroundDistance);
            pointY.set(0.0f, sideLength / 2.0f, backgroundDistance);

            if(
                    this.cam.frustum.pointInFrustum(pointX) == true ||
                    this.cam.frustum.pointInFrustum(pointY) == true
            ) {
                System.out.println("===== gg inside " + i);
                sideLength *= 1.1;
                bothPointsOutside = false;
            }
            else {
                System.out.println("===== gg outside");
                bothPointsOutside = true;
            }
            i++;
        }
        return sideLength;
    }

    private float wrap(float noiseValueHue) {
        if(noiseValueHue > 1.0f) {
            return noiseValueHue - 1.0f;
        }
        if(noiseValueHue < 0.0f) {
            return noiseValueHue + 1.0f;
        }
        return noiseValueHue;
    }

    public void initialiseShader() {
        this.disposeObjects();
        this.getAttributeAndUniformLocationsFull();
        this.getAttributeAndUniformLocationsFading();
    }

    public void disposeObjects() {
    }

    private void getAttributeAndUniformLocationsFull() {
        // light vertex attributes
        int positionLocation = this.shaderProgramLightsFull.getAttributeLocation("a_position");
        int colorLocation = this.shaderProgramLightsFull.getAttributeLocation("a_color");
        int distanceLocation = this.shaderProgramLightsFull.getAttributeLocation("a_distance");
        this.attributeLightsFullLocations = new int[] { positionLocation, colorLocation, distanceLocation };

        // background vertex attributes
        int positionBackgroundLocation = this.shaderProgramBackgroundFull.getAttributeLocation("a_position");
        int uvBackgroundLocation = this.shaderProgramBackgroundFull.getAttributeLocation("a_uv");
        this.attributeBackgroundFullLocations = new int[] { positionBackgroundLocation, uvBackgroundLocation };

        // light uniforms
        this.lightPosFullLocation = shaderProgramLightsFull.getUniformLocation("light_position");
        this.u_projModelViewFullLocation = shaderProgramLightsFull.getUniformLocation("u_projModelView");
        this.focalDistanceFullLocation = shaderProgramLightsFull.getUniformLocation("focal_distance");
        this.brightnessFullLocation = shaderProgramLightsFull.getUniformLocation("brightness_runtime");

        // background uniforms
        this.u_projModelViewBackgroundFullLocation = shaderProgramBackgroundFull.getUniformLocation("u_projModelView");
        this.brightnessBackgroundFullLocation = shaderProgramBackgroundFull.getUniformLocation("brightness");
    }

    private void getAttributeAndUniformLocationsFading() {
        // light vertex attributes
        int positionLocation = this.shaderProgramLightsFading.getAttributeLocation("a_position");
        int colorLocation = this.shaderProgramLightsFading.getAttributeLocation("a_color");
        int distanceLocation = this.shaderProgramLightsFading.getAttributeLocation("a_distance");
        this.attributeLightsFadingLocations = new int[] { positionLocation, colorLocation, distanceLocation };

        // background vertex attributes
        int positionBackgroundLocation = this.shaderProgramBackgroundFading.getAttributeLocation("a_position");
        int uvBackgroundLocation = this.shaderProgramBackgroundFading.getAttributeLocation("a_uv");
        this.attributeBackgroundFadingLocations = new int[] { positionBackgroundLocation, uvBackgroundLocation };

        // light uniforms
        this.lightPosFadingLocation = shaderProgramLightsFading.getUniformLocation("light_position");
        this.u_projModelViewFadingLocation = shaderProgramLightsFading.getUniformLocation("u_projModelView");
        this.focalDistanceFadingLocation = shaderProgramLightsFading.getUniformLocation("focal_distance");
        this.brightnessFadingLocation = shaderProgramLightsFading.getUniformLocation("brightness_runtime");
        this.fadeBrightnessLightFadingLocation = shaderProgramLightsFading.getUniformLocation("fade_brightness");

        // background uniforms
        this.u_projModelViewBackgroundFadingLocation = shaderProgramBackgroundFading.getUniformLocation("u_projModelView");
        this.brightnessBackgroundFadingLocation = shaderProgramBackgroundFading.getUniformLocation("brightness");
        this.fadeBrightnessBackgroundFadingLocation = shaderProgramBackgroundFading.getUniformLocation("fade_brightness");
    }
}
