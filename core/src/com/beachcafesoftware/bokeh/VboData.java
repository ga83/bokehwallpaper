package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.graphics.Texture;

public class VboData {

    // the float array which will be uploaded to the GPU. in this class, it is uploaded per-frame. in the retained subclass, it is uploaded once only, and then called upon per-frame.
    public float[] vboVerts;
    public int vboIndex;

    // VboData contains one Texture, and the list of Triangle's are all associated with the one Texture.
    public Texture texture;
    public KirraArrayList<Triangle> triangles;    // for buttons, will only be 2 triangles, but for say textured gems, can be a lot more


    public VboData() {
        // only exists to allow VboDataRetained
    }

}
