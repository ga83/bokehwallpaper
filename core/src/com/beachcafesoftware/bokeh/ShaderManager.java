package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.HashMap;

/**
 * This class does the work of managing ShaderPrograms, including caching them to save cpu time, plus any future functionality.
 */
public class ShaderManager {

    // The shader cache.
    private HashMap<ShaderParametersKey, ShaderParametersValue> cachedShaders;

    public ShaderProgram getShader(String type, boolean scaling, boolean fading) {

        ShaderParametersKey shaderParametersKey = new ShaderParametersKey(type, scaling, fading);

        if(this.cachedShaders.get(shaderParametersKey).shaderProgram != null) {

        //    System.out.println("===== sm: shader exists, returning cached, " + shaderParametersKey.hashCode());
            return cachedShaders.get(shaderParametersKey).shaderProgram;

        } else {
         //   System.out.println("===== sm: shader does not exist, building: " + type + ", " + scaling + ", " + fading + ", " + shaderParametersKey.hashCode());

            ShaderProgram shaderProgram;
            ShaderParametersValue shaderParametersValue = this.cachedShaders.get(shaderParametersKey);
            FileHandle vertexShaderFileHandle = Gdx.files.internal(shaderParametersValue.vertexShaderFileName);
            String vertexShaderString = vertexShaderFileHandle.readString();
            FileHandle fragmentShaderFileHandle = Gdx.files.internal(shaderParametersValue.fragmentShaderFileName);
            String fragmentShaderString = fragmentShaderFileHandle.readString();

            ShaderProgram.pedantic = false;
            shaderProgram = new ShaderProgram(vertexShaderString, fragmentShaderString);
            String log = shaderProgram.getLog();

            if (log != null) {
                System.out.println("Shader compile log: " + log);
            }

            if (shaderProgram.isCompiled() == false) {
                throw new GdxRuntimeException(log);
            }

         //   System.out.println("===== putting ShaderProgram hashcode(): " + shaderParametersKey.hashCode());
            shaderParametersValue.shaderProgram = shaderProgram;
            this.cachedShaders.put(shaderParametersKey, shaderParametersValue);

            return shaderProgram;
        }
    }

    public ShaderManager() {
        initialiseCache();
    }

    private void initialiseCache() {
        this.cachedShaders = new HashMap<ShaderParametersKey, ShaderParametersValue>();

        // light shaders
        this.cachedShaders.put(new ShaderParametersKey("light", false, false), new ShaderParametersValue("vertex-bokeh-noscale.glsl", "fragment-bokeh.glsl"));
        this.cachedShaders.put(new ShaderParametersKey("light", false, true), new ShaderParametersValue("vertex-bokeh-noscale-fading.glsl", "fragment-bokeh-fading.glsl"));
        this.cachedShaders.put(new ShaderParametersKey("light", true,  false), new ShaderParametersValue( "vertex-bokeh-scale.glsl", "fragment-bokeh.glsl"));
        this.cachedShaders.put(new ShaderParametersKey("light", true,  true), new ShaderParametersValue("vertex-bokeh-scale-fading.glsl", "fragment-bokeh-fading.glsl"));

        // background shaders
        this.cachedShaders.put(new ShaderParametersKey("background", false,  false), new ShaderParametersValue("vertex-bokeh-background.glsl", "fragment-bokeh-background.glsl"));
        this.cachedShaders.put(new ShaderParametersKey("background", false,  true), new ShaderParametersValue("vertex-bokeh-background-fading.glsl", "fragment-bokeh-background-fading.glsl"));
        this.cachedShaders.put(new ShaderParametersKey("background", true,  false), new ShaderParametersValue("vertex-bokeh-background.glsl", "fragment-bokeh-background.glsl"));
        this.cachedShaders.put(new ShaderParametersKey("background", true,  true), new ShaderParametersValue("vertex-bokeh-background-fading.glsl", "fragment-bokeh-background-fading.glsl"));

      //  System.out.println("===== sm: initialised");
    }

}
