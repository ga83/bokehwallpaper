package com.beachcafesoftware.bokeh;

import java.util.ArrayList;

/**
 * Created by Guy Aaltonen on 23/01/18.
 */

public interface BokehRenderer {
    void updateRibbons(int numberOfUpdatesToDo, ArrayList<Formation> activeFormations);
    void updateLights1(int numberOfUpdatesToDo);
    void renderLights(float sceneBrightness);
    void resetCamera();
}
