package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.math.Vector3;

public class DownStraightConstantMotionStrategy extends MotionStrategy {
    private final Vector3 initialVelocity;
    private final Vector3 initialAcceleration;

   public DownStraightConstantMotionStrategy(float motionSpeed) {
        this.initialVelocity = new Vector3(0, 0 - motionSpeed, 0);
        this.initialAcceleration = new Vector3(0, 0, 0);
    }

    public void initialise(Light light, float dt) {
        light.velocity = this.initialVelocity.cpy();
        light.acceleration = this.initialAcceleration.cpy();
    }

    @Override
    public void update(Light light, float dt) {

    }

}
