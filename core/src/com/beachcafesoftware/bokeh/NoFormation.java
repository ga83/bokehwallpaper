package com.beachcafesoftware.bokeh;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 1/11/17.
 */

public class NoFormation implements Formation {

    private ArrayList<Ribbon> ribbons;
    private int lastReplacedIndex;


    public NoFormation() {
        this.ribbons = new ArrayList<Ribbon>();
        this.lastReplacedIndex = 0;
        Ribbon.MAX_DELTA_ROTATE = 8.0f + new Random().nextFloat() * 8.0f;
    }

    @Override
    public void removeAllRibbons() {
        this.ribbons.clear();
    }

    @Override
    public void addRibbons(ArrayList<Ribbon> ribbons,float speedMult) {
        for(int i = 0; i < ribbons.size(); i++) {
            ribbons.get(i).setRandomSpeed(speedMult);
        }
        this.ribbons.addAll(ribbons);
    }

    @Override
    public void setSpeed(float speed) {
    }

    @Override
    public void updateAllRibbons(float dt) {
        for(int i = 0; i < ribbons.size(); i++) {
            Ribbon r = ribbons.get(i);
            updateIndividualRibbon(dt, r);
        }
    }


    @Override
    public ArrayList<Ribbon> getRibbons() {
        return ribbons;
    }

    @Override
    public int getNextReplacedIndex() {
        this.lastReplacedIndex ++;
        if(this.lastReplacedIndex >= this.ribbons.size()) {
            this.lastReplacedIndex = 0;
        }

        return lastReplacedIndex;
    }

    @Override
    public int getMinimumFormations() {
        return 1;
    }

    @Override
    public int getMaximumFormations() {
        return 1;
    }

    public void updateIndividualRibbon(float dt,Ribbon ribbon) {

        ribbon.position.x += ribbon.velocity.x * dt;
        ribbon.position.y += ribbon.velocity.y * dt;
        ribbon.position.z += ribbon.velocity.z * dt;

        ribbon.randomiseRotationAxis(ribbon.rotationAxis, 0.0f * dt, 90.0f * dt);
        int direction;      // just negative or positive
        direction = ribbon.rnd.nextBoolean() ? 1 : -1;
        ribbon.turningAngle += Ribbon.MAX_DELTA_ROTATE * dt * ribbon.rnd.nextFloat() * direction * (ribbon.speed / 450f);

        if(ribbon.returningHome == false) {
            ribbon.velocity.rotate(ribbon.rotationAxis, ribbon.turningAngle);
        }
        // ease out to avoid tight loops
        ribbon.turningAngle = ribbon.turningAngle * (1 - (dt * 0.1f)); // TODO: MIGHT need change of constant 0.1f for different framerates

        if(
            (ribbon.position.x > Ribbon.BOUNDARY_RIGHT ) ||
            (ribbon.position.x < Ribbon.BOUNDARY_LEFT  ) ||
            (ribbon.position.y > Ribbon.BOUNDARY_BOTTOM) ||
            (ribbon.position.y < Ribbon.BOUNDARY_TOP   ) ||
            (ribbon.position.z > Ribbon.BOUNDARY_BACK  ) ||
            (ribbon.position.z < Ribbon.BOUNDARY_FRONT )
        )    {
            ribbon.accelerateHome();
        }
        ribbon.approachSpeedLimit();
    }



}
