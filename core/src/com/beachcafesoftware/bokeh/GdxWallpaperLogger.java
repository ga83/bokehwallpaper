package com.beachcafesoftware.bokeh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;

/**
 * Created by Guy Aaltonen on 5/09/18.
 */

public class GdxWallpaperLogger implements AppUsageLogger, Net.HttpResponseListener {

    String url = null;
    String applicationName = null;

    @Override
    public void log() throws ParametersNotSetException {

        if(url == null || applicationName == null) {
            throw new ParametersNotSetException();
        }

        else {
            final GdxWallpaperLogger gl = this;
            HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
            Net.HttpRequest request = requestBuilder.newRequest().method(Net.HttpMethods.GET).url(url + "?application=" + applicationName + "&interval=" + -1).build();
            request.setTimeOut(1000 * 20);
            Gdx.net.sendHttpRequest(request, gl);
        }
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setApplicationName(String appname) {
        this.applicationName = appname;
    }

    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {
        System.out.println("===== got response");
    }

    @Override
    public void failed(Throwable t) {
        System.out.println("===== failed " + t.getMessage());
    }

    @Override
    public void cancelled() {
        System.out.println("===== cancelled");
    }
}
